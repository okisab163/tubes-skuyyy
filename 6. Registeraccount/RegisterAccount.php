<html>

	<head>

		<title> Register Account </title>

		<style>


			* {

				margin : 0;
				padding : 0;
			}

			@font-face {


				font-family: "acremedium";

				src: url('../Resources/fonts/jonathan_ball_-_acre-medium-webfont.woff2') format('woff2'),

				url('../Resources/fonts/jonathan_ball_-_acre-medium-webfont.woff') format('woff');

				font-weight: normal;

				font-style: normal;

			}

			@font-face {
    

				font-family : "holtwood_one_scregular";

				src : url("../Resources/fonts/holtwoodonesc-webfont.woff2") format("woff2"),

				url("../Resources/fonts/holtwoodonesc-webfont.woff") format("woff");
				font-weight : normal;

				font-style : normal;
			
			}


			.BackgroundAtas {

				background-image: url("../Resources/images/Background.png");
				height: 150%;
				background-position: center;
				background-repeat: no-repeat;
				background-size: cover;
				position: relative;
				width : 100%;

			}

			.BackgroundBawah {

				background-image: url("../Resources/images/Background1.png");
				height: 50%;
				background-position: center;
				background-repeat: no-repeat;
				background-size: cover;
				position: relative;
				width : 100%;
				z-index: -1;

			}

			.LogoUtama {

				margin-top : 40px;
				margin-left : 50px;
				width : 150px;
				-webkit-filter : drop-shadow(0px 3px 3px #000000);
				filter : drop-shadow(0px 3px 3px #000000); 

			}

			fieldset {

				width : 700px;
				height: 1150px;
				background : #FFFFFF 0% 0% no-repeat padding-box;
				box-shadow : 10px 10px 6px #0000004F;
				border : 2px solid #FFA200;
				border-radius : 25px;
				opacity : 1;

			}

			.ContainerLogo {

				margin-top : 40px;
				margin-left : 200px;
				position : relative;
				width : 200px;
				height : 230px;

			}

			.ContainerLogoDepan {

				position: absolute;
				z-index: 2;
			}

			.ContainerLogoBelakang {

				position: absolute;
				z-index: 1;

			}

			.LogoKucing {

				width : 110px;
				-webkit-filter : drop-shadow(0px 3px 3px #000000);
				filter : drop-shadow(0px 3px 3px #000000);

			}

			.IconRegister {

				font-family : holtwood_one_scregular;
				letter-spacing : -3px;
				color : #4C9AE9;
				text-shadow : 0px 8px 4px #00000070;
				font-size : 58px;
				position : absolute;
				margin-top : 75px;
				left : -500px;
				width: 1000px;

			}

			::-webkit-input-placeholder {

				color: #00000029;
				font-family : acremedium;
				font-size : 15px;

			}

			.ContainerName {

				width: 500px;
				height: 53px;

			}

			.NameKiri {

				float : left;
				width : 100px;

			}

			.NameKanan {

				float-right;
				margin-right: -260px;
				width : 220px;

			}

			.Firstname {

				width : 250px;
				height : 50px;
				box-shadow : 3px 3px 6px #00000029;
				border : 1px solid #00000029;
				border-radius : 15px;
				padding-left : 30px;
				font-family : acremedium;
				font-size : 15px;

			}

			.Firstname:focus {

				outline: none;

			}

			.LastName {

				width : 230px;
				height : 50px;
				box-shadow : 3px 3px 6px #00000029;
				border : 1px solid #00000029;
				border-radius : 15px;
				padding-left : 30px;
				font-family : acremedium;
				font-size : 15px;

			}

			.LastName:focus {

				outline: none;

			}

			.Username {

				margin-top : 5px;
				width : 500px;
				height : 50px;
				box-shadow : 3px 3px 6px #00000029;
				border : 1px solid #00000029;
				border-radius : 15px;
				padding-left : 30px;
				padding-top : 5px;
				font-family : acremedium;

			}

			.Username:focus {

				outline: none;

			}

			.Email {

				margin-top : 25px;
				width : 500px;
				height : 50px;
				box-shadow : 3px 3px 6px #00000029;
				border : 1px solid #00000029;
				border-radius : 15px;
				padding-left : 30px;
				padding-top : 5px;
				font-family : acremedium;

			}

			.Email:focus {

				outline: none;

			}

			.Address {

				margin-top : 25px;
				width : 500px;
				height : 50px;
				box-shadow : 3px 3px 6px #00000029;
				border : 1px solid #00000029;
				border-radius : 15px;
				padding-left : 30px;
				padding-top : 5px;
				font-family : acremedium;

			}

			.Address:focus {

				outline: none;

			}

			.PhoneNumber {

				margin-top : 25px;
				width : 500px;
				height : 50px;
				box-shadow : 3px 3px 6px #00000029;
				border : 1px solid #00000029;
				border-radius : 15px;
				padding-left : 30px;
				padding-top : 5px;
				font-family : acremedium;

			}

			.PhoneNumber:focus {

				outline: none;

			}

			.Password {

				margin-top : 25px;
				width : 500px;
				height : 50px;
				box-shadow : 3px 3px 6px #00000029;
				border : 1px solid #00000029;
				border-radius : 15px;
				padding-left : 30px;
				padding-top : 5px;

			}

			.Password:focus {

				outline: none;

			}

			.LabelBirth {

				margin-top : 25px;
				margin-right: 360px;
				width : 100px;

			}

			.BirthDate {

				margin-top : 50px;
				font-family : acremedium;
				font-size : 15px;
				color: #00000029;

			}

			.ContainerDate {

				width: 500px;
				height: 53px;

			}

			.DateDayKiri {

				float : left;
				width : 150px;

			}

			.DateMonthTengah {

				margin:auto;
				width : 150px;

			}

			.DateYearKanan {

				float: right;
				width : 150px;

			}

			.SelectDay {

				margin-right : 30px;
				width : 150px;
				height : 50px;
				box-shadow : 3px 3px 6px #00000029;
				border : 1px solid #00000029;
				border-radius : 15px;
				text-align-last:center;
				font-family : acremedium;
				font-size : 15px;

			}

			.SelectDay:focus {

				outline: none;

			}

			.SelectMonth {

				width : 150px;
				height : 50px;
				box-shadow : 3px 3px 6px #00000029;
				border : 1px solid #00000029;
				border-radius : 15px;
				text-align-last:center;
				font-family : acremedium;
				font-size : 15px;

			}

			.SelectMonth:focus {

				outline: none;

			}

			.SelectYear {

				width : 150px;
				height : 50px;
				box-shadow : 3px 3px 6px #00000029;
				border : 1px solid #00000029;
				border-radius : 15px;
				text-align-last:center;
				font-family : acremedium;
				font-size : 15px;

			}

			.SelectYear:focus {

				outline: none;

			}

			.LabelGender {

				margin-top : 5px;
				margin-right: 360px;
				width : 100px;

			}

			.Gender {

				margin-top : 50px;
				font-family : acremedium;
				font-size : 15px;
				color: #00000029;

			}

			.ContainerGender {

				margin-top : 5px;
				width : 558px;
				height : 30px;

			}

			.GenderKiri {

				float : left;
				width : 150px; 

			}

			.GenderKanan {

				float : left;
				width : 150px; 
				left: 50px;

			}

			.Radio1 {

				transform: scale(3);
				width : 50px;
				cursor: pointer;

			}

			.Radio1:focus {

				outline: none;

			}

			.Radio2 {

				transform: scale(3);
				width : 50px;
				cursor: pointer;

			}

			.Radio2:focus {

				outline: none;

			}

			.Male {

				font-family : acremedium;
				font-size : 18px;
				color: #00000029;

			}

			.Female {

				font-family : acremedium;
				font-size : 18px;
				color: #00000029;

			}

			.SignIn {

				margin-top : 25px;
				width: 130px;
				height: 50px;
				background: #FF8000;
				box-shadow : 3px 3px 6px #00000073;
				border : 5px solid #FF8000;
				border-radius : 15px;
				position : relative;
				top : 3px;
				text-align : center;
				font-family : acremedium;
				font-size : 15px;
				color : #000000;
				cursor: pointer;

			}

			.SignIn:focus {

				outline: none;

			}

			.ContainerTerms {

				margin-top : 40px;

			}

			.Terms1 {

				font-family : acremedium;
				font-size : 26px;
				color : #000000;

			}

			.Terms2 {

				font-family : acremedium;
				font-size : 26px;
				color : #000000;

			}

		</style>

	</head>

	<body>

		<div class = "BackgroundAtas">

			<a href = "..\1. Home\Home.php"> <img src = "../Resources/images/LOGOANML.png" class = "LogoUtama"> </a>

			<center>

				<fieldset>

					<form action = "commandRegister.php" method = "POST">

						<div class = "ContainerLogo">

							<div class = "ContainerLogoDepan"> <img src = "../Resources/images/58a050065583a1291368eeb4.png" class = "LogoKucing"> </div>

							<div class = "ContainerLogoBelakang"> <label class = "IconRegister"> REGISTER ACCOUNT </label> </div>


						</div>

						<div class = "ContainerName">

							<div class = "NameKiri"> <input type = "text" class = "Firstname" placeholder = "First Name" name = "Firstname"> </div>

							<div class = "NameKanan"> <input type = "text" class = "LastName" placeholder = "Last Name" name = "Lastname"> </div>

						</div>

							<br>

						<input type = "text" class = "Username" placeholder = "Username" name = "Username">

							<br>

						<input type = "text" class = "Email" placeholder = "Email" name = "Email">

							<br>

						<input type = "text" class = "Address" placeholder = "Address" name = "Address">

							<br>

						<input type = "number" class = "PhoneNumber" placeholder = "PhoneNumber" name = "PhoneNumber">

							<br>

						<input type = "password" class = "Password" placeholder = "Password" name = "Password">

							<br>

						<div class = "LabelBirth"> <label class = "BirthDate"> <b> Birth Date </b> </label> </div>

							<br>

						<div class = "ContainerDate">

						<div class = "DateDayKiri">

							<select name = "day" id = "day" class = "SelectDay">

									<option disabled selected hidden> DD </option>
									<option value="1"> 1 </option>
									<option value="2"> 2 </option>
									<option value="3"> 3 </option>
									<option value="4"> 4 </option>
									<option value="5"> 5 </option>
									<option value="6"> 6 </option>
									<option value="7"> 7 </option>
									<option value="8"> 8 </option>
									<option value="9"> 9 </option>
									<option value="10"> 10 </option>
									<option value="11"> 11 </option>
									<option value="12"> 12 </option>
									<option value="13"> 13 </option>
									<option value="14"> 14 </option>
									<option value="15"> 15 </option>
									<option value="16"> 16 </option>
									<option value="17"> 17 </option>
									<option value="18"> 18 </option>
									<option value="19"> 19 </option>
									<option value="20"> 20 </option>
									<option value="21"> 21 </option>
									<option value="22"> 22 </option>
									<option value="23"> 23 </option>
									<option value="24"> 24 </option>
									<option value="25"> 25 </option>
									<option value="26"> 26 </option>
									<option value="27"> 27 </option>
									<option value="28"> 28 </option>
									<option value="29"> 29 </option>
									<option value="30"> 30 </option>
									<option value="31"> 31 </option>

								</select>

							</div>

							<div class = "DateYearKanan">

								<select name = "year" id = "year" class = "SelectYear">

									<option disabled selected hidden> YYYY </option>
									<option value="1970"> 1970 </option>
									<option value="1971"> 1971 </option>
									<option value="1972"> 1972 </option>
									<option value="1973"> 1973 </option>
									<option value="1974"> 1974 </option>
									<option value="1975"> 1975 </option>
									<option value="1976"> 1976 </option>
									<option value="1977"> 1977 </option>
									<option value="1978"> 1978 </option>
									<option value="1979"> 1979 </option>
									<option value="1980"> 1980 </option>
									<option value="1981"> 1981 </option>
									<option value="1982"> 1982 </option>
									<option value="1983"> 1983 </option>
									<option value="1984"> 1984 </option>
									<option value="1985"> 1985 </option>
									<option value="1986"> 1986 </option>
									<option value="1987"> 1987 </option>
									<option value="1988"> 1988 </option>
									<option value="1989"> 1989 </option>
									<option value="1990"> 1990 </option>
									<option value="1991"> 1991 </option>
									<option value="1992"> 1992 </option>
									<option value="1993"> 1993 </option>
									<option value="1994"> 1994 </option>
									<option value="1995"> 1995 </option>
									<option value="1996"> 1996 </option>
									<option value="1997"> 1997 </option>
									<option value="1998"> 1998 </option>
									<option value="1999"> 1999 </option>
									<option value="2000"> 2000 </option>
									<option value="2001"> 2001 </option>
									<option value="2002"> 2002 </option>
									<option value="2003"> 2003 </option>
									<option value="2004"> 2004 </option>
									<option value="2005"> 2005 </option>

								</select>

							</div>

							<div class = "DateMonthTengah">

								<select name = "month" id = "month" class = "SelectMonth">

									<option disabled selected hidden> MM </option>
									<option value="1"> January </option>
									<option value="2"> February </option>
									<option value="3"> March </option>
									<option value="4"> April </option>
									<option value="5"> May </option>
									<option value="6"> June </option>
									<option value="7"> July </option>
									<option value="8"> August </option>
									<option value="9"> September </option>
									<option value="10"> October </option>
									<option value="11"> November </option>
									<option value="12"> December </option>

								</select>

							</div>

						</div>

							<br>

						<div class = "LabelGender"> <label class = "Gender"> <b> Gender </b> </label> </div>

							<br>

						<div class = "ContainerGender">

							<div class = "GenderKiri"> <input type = "radio" name = "Gender" value = "Male" class = "Radio1"> <label class = "Male"> <b> Male </b> </label> </div>

							<div class = "GenderKanan"> <input type = "radio" name = "Gender" value = "Female" class = "Radio2"> <label class = "Female"> <b> Female </b> </label> </div>

						</div>

							<br>

						<button class = "SignIn"  name = "SignUp"> <b> Sign Up </b> </button>

							<br>

						<div class = "ContainerTerms">

							<label class = "Terms1"> With clicking Sign In you will agree to our policies, </label>

								<br>

							<label class = "Terms2"> terms and conditions </label>

						</div>

					</form>

				</fieldset>

			</center>

	</div>

	<div class = "BackgroundBawah">

	</div>

	</body>

</html>