<?php

	include("ConfigLocal.php");
	
	if(isset($_POST['ButtonAddAnimals'])) {

    	if(!isset($_FILES['ImagesAdd']['tmp_name'])) {

			echo "	<script>
					alert('Pilih File Gambar Terlebih Dahulu');
					location.replace('Adoptapet(admin).php')
					</script>";

    	} else {

        	$file_name = $_FILES['ImagesAdd']['name'];
        	$file_size = $_FILES['ImagesAdd']['size'];
       		$file_type = $_FILES['ImagesAdd']['type'];
			$file_tmp = $_FILES['ImagesAdd']['tmp_name'];

        	if ($file_size < 2048000 and ($file_type =='image/jpeg' or $file_type == 'image/png')) {

				$DateArrived = $_POST['DateArrivedAdd'];
				$AnimalName = $_POST['AnimalNameAdd'];
				$Breed = $_POST['BreedAdd'];
				$SexAnimal = $_POST['AnimalSexAdd'];
				$AnimalAge = $_POST['AnimalAgeAdd'];
				$Description = $_POST['DescriptionAdd'];

				$Species = $_POST['SpeciesAdd'];

				if ($Species === "Dogs") {

					$sql = "INSERT INTO pet_adopt_dog VALUES (null, '$file_name', '$DateArrived', '$AnimalName', '$Breed', '$SexAnimal', '$AnimalAge', '$Description')";

					move_uploaded_file($file_tmp, '../Server Uploads/Adopt Dog/'.$file_name);

            				$query = mysqli_query($conn, $sql) or die(mysqli_error($conn));

					if ($query) {

						echo "	<script>
								alert('Data Berhasil Dimasukan');
								location.replace('Adoptapet(admin).php')
								</script>";
					} else {

						echo "	<script>
								alert('Terdapat Error Di');
								location.replace('Adoptapet(admin).php')
								</script>";		

					}

				} else if ($Species === "Cats") {

					$sql = "INSERT INTO pet_adopt_cat VALUES (null, '$file_name', '$DateArrived', '$AnimalName', '$Breed', '$SexAnimal', '$AnimalAge', '$Description')";

					move_uploaded_file($file_tmp, '../Server Uploads/Adopt Cat/'.$file_name);

            		$query = mysqli_query($conn, $sql) or die(mysqli_error($conn));

					if ($query) {

						echo "	<script>
								alert('Data Berhasil Dimasukan');
								location.replace('Adoptapet(admin).php')
								</script>";
					} else {

						echo "	<script>
								alert('Terdapat Error Di');
								location.replace('Adoptapet(admin).php')
								</script>";		

					}

				} else {

					$sql = "INSERT INTO pet_adopt_other VALUES (null, '$file_name', '$DateArrived', '$AnimalName', '$Breed', '$SexAnimal', '$AnimalAge', '$Description')";

					move_uploaded_file($file_tmp, '../Server Uploads/Adopt Other/'.$file_name);

            		$query = mysqli_query($conn, $sql) or die(mysqli_error($conn));

					if ($query) {

						echo "	<script>
								alert('Data Berhasil Dimasukan');
								location.replace('Adoptapet(admin).php')
								</script>";
					} else {

						echo "	<script>
								alert('Terdapat Error Di');
								location.replace('Adoptapet(admin).php')
								</script>";		

					}

				}

        	} else {

				echo "	<script>
						alert('Tipe File Gambar Tidak Sesuai Seharusnya JPEG dan PNG');
						location.replace('Adoptapet(admin).php')
						</script>";

        	}

    	}

	}

?>