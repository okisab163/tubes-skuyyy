<?php

	session_start();

?>

<html>

	<head>
	
		<title> Adopt A Pet (2) </title>

		<style>

			* {

				margin : 0;
				padding : 0;
			}

			@font-face {

				font-family: "acremedium";

				src: url('../Resources/fonts/jonathan_ball_-_acre-medium-webfont.woff2') format('woff2'),

				url('../Resources/fonts/jonathan_ball_-_acre-medium-webfont.woff') format('woff');

				font-weight: normal;

				font-style: normal;

			}

			@font-face {

				font-family : "holtwood_one_scregular";

				src : url("../Resources/fonts/holtwoodonesc-webfont.woff2") format("woff2"),

				url("../Resources/fonts/holtwoodonesc-webfont.woff") format("woff");
				font-weight : normal;

				font-style : normal;

			}

			@font-face {

				font-family: 'autour_oneregular';
				src: url('../Resources/fonts/autourone-regular-webfont.woff2') format('woff2'),
				url('../Resources/fonts/autourone-regular-webfont.woff') format('woff');
				font-weight: normal;
				font-style: normal;

			}

			@font-face {

				font-family: 'primerprint';
				src: url('../Resources/fonts/primer_print-webfont.woff2') format('woff2'),
				url('../Resources/fonts/primer_print-webfont.woff') format('woff');
				font-weight: normal;
				font-style: normal;

			}

			.BackgroundAtas {

				background-image: url("../Resources/images/Violet+Simple+Class+Schedule_upscaled_illustration_x4.png");
				height: 230%;
				background-position: center;
				background-repeat: no-repeat;
				background-size: cover;
				position: relative;
				width : 100%;

			}

			.ContainerNavbar {

				height: 10px;

			}

			.NavbarKiri {

				float: left;

			}

			.NavbarKanan {

				margin-top: 40px;
				width: 800px;
				float: right;

			}

			.TulisanNavbar {

				font-family: acremedium;
				text-decoration: none;
				font-size: 33px;
				letter-spacing: 0;
				color: #E54A4A;
				opacity: 1;

			}

			.TulisanNavbar:hover {

				font-family: acremedium;
				text-decoration: none;
				font-size: 33px;
				letter-spacing: 0;
				color: #F34D14;
				opacity: 1;

			}

			.TulisanNavbarTerpilih {

				font-family: acremedium;
				text-decoration: none;
				font-size: 33px;
				letter-spacing: 0;
				color: #4AA7E5;
				opacity: 1;

			}

			.LogoUtama {

				margin-top : 40px;
				margin-left : 50px;
				width : 150px;
				-webkit-filter : drop-shadow(0px 3px 3px #000000);
				filter : drop-shadow(0px 3px 3px #000000); 

			}

			fieldset {

				width : 1200px;
				height: 1000px;
				margin-top: 440px;
				background : #FFFFFF 0% 0% no-repeat padding-box;
				box-shadow : 10px 10px 6px #0000004F;
				border : 2px solid #FFA200;
				border-radius : 25px;
				opacity : 1;

			}

			.ContainerLogo {

				margin-top : 40px;
				margin-left : 200px;
				position : relative;
				width : 200px;
				height : 230px;
				position: absolute;
				top: 210px;
				right: 100px;

			}

			.ContainerLogoDepan {

				position: absolute;
				z-index: 2;
			}

			.ContainerLogoBelakang {

				position: absolute;
				z-index: 1;
				left: -500px;
				width: 500px;
			}

			.LogoKucing {

				width : 110px;
				-webkit-filter : drop-shadow(0px 3px 3px #000000);
				filter : drop-shadow(0px 3px 3px #000000);

			}

			.IconRegister {

				font-family : holtwood_one_scregular;
				letter-spacing : -3px;
				color : #4C9AE9;
				text-shadow : 0px 8px 4px #00000070;
				font-size : 60px;
				position : absolute;
				margin-top : 75px;
				right : -104px;

			}

			::-webkit-input-placeholder {

				color: #00000029;
				font-family : acremedium;
				font-size : 15px;

			}

			.ContainerLabelThanks {

				height: 500px;
				width: 1000px;
				margin-top: 100px;
				background-color: #EB7D00;
				box-shadow: 10px 10px 6px #0000004F;
				border: 5px solid #000000;
				border-radius: 50px;
				text-align: left;
				font-family : acremedium;
				font-size : 28px;
				letter-spacing: 0;
				color: black;
				opacity: 1;

			}

			.ButtonOk {

				width: 300px;
				height: 70px;
				margin-top: 30px;
				margin-right:100px;
				float: right;
				background-color: #EB7D00;
				box-shadow: 3px 3px 6px #00000073;
				border: 5px solid #FF8000;
				border-radius: 14px;
				cursor: pointer;
				opacity: 1;
				font-family : acremedium;
				font-size : 24px;
				letter-spacing: 0;
				color: black;

			}

			.bgFooter {

				margin-top: 100px;
 				background-image: url("../Resources/images/footer.jpg");
				background-repeat: no-repeat;
				background-size: 100% 100%;
				width : 100%;
				height: 550px;
			}
	
			.LabelLearn {

				color: #FEBF56;
				font-family: primerprint;
				font-size: 15px;

			}

			.LabelMore {

				color: black;
				font-family: primerprint;
				font-size: 42px;
				text-decoration: none;

			}

			.LabelMore:hover {

				color: #FFAA00;
				font-family: primerprint;
				font-size: 42px;
				text-decoration: none;

			}

			.LabelLove {

				color: black;
				font-family: autour_oneregular;
				font-size: 44px;

			}

			.CloserLabel {

				text-align: center;
				font-family : acremedium;
				font-size : 48px;
				color: #0C78CB;

			}

			.dropbtn {

				border: none;
				font-family: acremedium;
				text-decoration: none;
				font-size: 33px;
				letter-spacing: 0;
				color: #E54A4A;
				opacity: 1;
				margin-right: 14px;
				background-color: Transparent;
    			background-repeat:no-repeat;
    			border: none;
    			cursor:pointer;

			}

			.dropdown {

				display: inline-block;
			}

			.dropdown-content {

				display: none;
				position: absolute;
				background-color: #f1f1f1;
				min-width: 160px;
				box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
				z-index: 1;
			}

			.dropdown-content a {

				color: black;
				font-size: 20px;
				letter-spacing: 0;
				padding: 12px 16px;
				text-decoration: none;
				display: block;
			}

			.dropdown-content a:hover {

				background-color: #ddd;

			}

			.dropdown:hover .dropdown-content {

				display: block;

			}

		</style>

	</head>

	<body>

		<div class = "BackgroundAtas">

			<div class = "ContainerNavbar">

				<div class = "NavbarKiri">

					<a href = "..\13. Home (User)\Home (User).php"> <img src = "../Resources/images/LOGOANML.png" class = "LogoUtama"> </a>

				</div>

				<div class = "NavbarKanan">

					<div style = "float: left;"> 

						<center>

							<a href = "Adoptapet(1) (User).php" class = "TulisanNavbarTerpilih"> Adopt <br> a pet </a> 

						</center>

					</div>

					<div style = "float: left; margin-left: 85px;">

						<center>

							<a href = "..\16. Lost and Found (User)\Lostandfound (User).php" class = "TulisanNavbar"> Lost <br> and found </a>

						</center>

					</div>

					<div style = "float: left; margin-left: 85px; margin-top: 20px;">

						<center>

							<a href = "..\17. About us (User)\Aboutus (User).php" class = "TulisanNavbar"> About Us </a>

						</center>

					</div>

					<div style = "float: left; margin-left: 60px; margin-top: 20px;">

						<center>

							<div class="dropdown">

								<button class="dropbtn"> <?= $_SESSION["username"] ?> </button>

								<div class="dropdown-content">

									<a href = "..\19. Edit Account (User)\UpdateAccount (User).php"> Update Profil </a>
									<a href = "logout.php">Log Out</a>

								</div>

							</div>

						</center>

					</div>

				</div>

			</div>

			<center>

				<div class = "ContainerLogo">

					<div class = "ContainerLogoDepan"> <img src = "../Resources/images/58a050065583a1291368eeb4.png" class = "LogoKucing"> </div>

					<div class = "ContainerLogoBelakang"> <label class = "IconRegister"> ADOPT A PET </label> </div>


				</div>

				<fieldset>

					<center>

						<div style = "margin-top: 70px;">

							<label class = "CloserLabel"> You are Getting Closer to Your New <br> Family Member! </label>

						</div>

						<div class = "ContainerLabelThanks">

							<div style = "margin-top: 25px; margin-left: 30px;">

								<label> Thank you for your good intention to give a better life to a Love And Care Animals Shelter. </label>

							</div>

							<div style = "margin-top: 25px; margin-left: 30px;">

								<label>  In the adoption process, we will need information on your home environment and the lifestyle of your family. This information is needed so that we can assist you to give instruction for your new pet. </label>

							</div>

							<div style = "margin-top: 25px; margin-left: 30px;">

								<label> All information you provide will be kept confidential and only be used for the purposes of adoption.  </label>

							</div>

							<div style = "margin-top: 25px; margin-left: 30px;">

								<label> Make sure you have completed all the information requested before you send it to: <br> okisabeni@gmail.com </label>

							</div>

						</div>

					</center>

					<button class = "ButtonOk"> Ok </button>


				</fieldset>


			</center>

		<div class>

			<div class = "bgFooter">

				<div style = "float: right;">

					<div style = "margin-right: 70px; margin-top: 50px;"> 

						<label class = "LabelLearn"> © Copyright 2019, powered by puppet </label> 

					</div>

				</div>

				<div style = "display: inline-block; margin-top: 380px; ">

					
					<div style = "margin-top: 20px; float: right; margin-left: 140px;"> 

						<a href = "#" class = "LabelMore"> <b> Learn More </b> </a> 

					</div>

					<div style = "margin-top: 20px; float: right; margin-left: 110px;"> 

						<a href = "#" class = "LabelMore"> <b> Our Shelter </b> </a> 

					</div>

					<div style = "margin-top: 20px; float: right; margin-left: 130px;"> 

						<a href = "#" class = "LabelMore"> <b> Contact Us </b> </a> 

					</div>

					<div style = "margin-top: 20px; float: right; margin-left: 250px;"> 

						<a href = "#" class = "LabelMore"> <b> About Us </b> </a> 

					</div>

				</div>

			</div>

		</div>

	</body>

</html>