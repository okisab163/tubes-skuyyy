<?php

	session_start();

?>

<html>

	<head>
	
		<title> Home </title>

		<style>

			* {

				margin : 0;
				padding : 0;
			}

			@font-face {

				font-family: "acremedium";

				src: url('../Resources/fonts/jonathan_ball_-_acre-medium-webfont.woff2') format('woff2'),

				url('../Resources/fonts/jonathan_ball_-_acre-medium-webfont.woff') format('woff');

				font-weight: normal;

				font-style: normal;

			}

			@font-face {

				font-family : "holtwood_one_scregular";

				src : url("../Resources/fonts/holtwoodonesc-webfont.woff2") format("woff2"),

				url("../Resources/fonts/holtwoodonesc-webfont.woff") format("woff");
				font-weight : normal;

				font-style : normal;

			}

			@font-face {

				font-family: 'autour_oneregular';
				src: url('../Resources/fonts/autourone-regular-webfont.woff2') format('woff2'),
				url('../Resources/fonts/autourone-regular-webfont.woff') format('woff');
				font-weight: normal;
				font-style: normal;

			}

			@font-face {

				font-family: 'primerprint';
				src: url('../Resources/fonts/primer_print-webfont.woff2') format('woff2'),
				url('../Resources/fonts/primer_print-webfont.woff') format('woff');
				font-weight: normal;
				font-style: normal;

			}

			.BackgroundAtas {

				width : 100%;
				height: 300%;
				background-image: url("../Resources/images/Violet+Simple+Class+Schedule_upscaled_illustration_x4.png");
				background-position: center;
				background-repeat: no-repeat;
				background-size: cover;

			}

			.ContainerNavbar {

				height: 10px;

			}

			.NavbarKiri {

				float: left;

			}

			.NavbarKanan {

				margin-top: 40px;
				width: 800px;
				float: right;

			}

			.TulisanNavbar {

				font-family: acremedium;
				text-decoration: none;
				font-size: 33px;
				letter-spacing: 0;
				color: #E54A4A;
				opacity: 1;

			}

			.TulisanNavbar:hover {

				font-family: acremedium;
				text-decoration: none;
				font-size: 33px;
				letter-spacing: 0;
				color: #F34D14;
				opacity: 1;

			}

			.LogoUtama {

				margin-top : 40px;
				margin-left : 50px;
				width : 150px;
				-webkit-filter : drop-shadow(0px 3px 3px #000000);
				filter : drop-shadow(0px 3px 3px #000000); 

			}

			fieldset {

				width : 1200px;
				height: 1010px;
				margin-top: 300px;
				background : #FFFFFF;
				box-shadow : 10px 10px 6px #0000004F;
				border : 2px solid #FFA200;
				border-radius : 25px;
				opacity : 1;

			}

			::-webkit-input-placeholder {

				color: #00000029;
				font-family : acremedium;
				font-size : 15px;

			}

			.bgFooter {

 				background-image: url("../Resources/images/footer.jpg");
				background-repeat: no-repeat;
				background-size: 100% 100%;
				width : 100%;
				height: 550px;
			}
	
			.LabelLearn {

				color: #FEBF56;
				font-family: primerprint;
				font-size: 15px;

			}

			.LabelMore {

				color: black;
				font-family: primerprint;
				font-size: 42px;
				text-decoration: none;

			}

			.LabelMore:hover {

				color: #FFAA00;
				font-family: primerprint;
				font-size: 42px;
				text-decoration: none;

			}

			.LabelLove {

				color: black;
				font-family: autour_oneregular;
				font-size: 44px;

			}

			.slideshowContainer {

				position: relative;
				overflow: hidden;
				margin: 50px 0 75px;
				width: 100%;
				height: 700px;

			}

			.imageSlides {

				margin-left: 800px;
				margin-top: 300px;
				width: 10%;
				transform: translate(-50%, -50%);
				min-width: 100%;
				min-height: 100%;
				opacity: 0;
				transition: opacity 1s ease-in-out;
				z-index: -1;

			}

			.visible {

				opacity: 1;

			}

			.slideshowArrow {

				font-size: 7em;
				color: rgba(255, 255, 255, 0.5);
				cursor: pointer;
				transition: opacity 0.2s ease-in-out;
			}

			.slideshowArrow:hover {

				opacity: 0.75;

			}

			#leftArrow {

				position: absolute;
				color: #000000;
				left: 4%;
				top: 50%;
				transform: translate(-50%, -50%);

			}

			#rightArrow {

				position: absolute;
				color: #000000;
				right: 4%;
				top: 50%;
				transform: translate(50%, -50%);

			}

			.slideshowCircles {

				position: absolute;
				bottom: 2%;
				left: 50%;
				transform: translate(-50%, -50%);
				text-align: center;

			}

			.circle {

				display: inline-block;
				margin-left: 3px;
				margin-right: 3px;
				width: 15px;
				height: 15px;
				border-radius: 50%;
				border: solid 2px rgba(255, 255, 255, 0.5);
				transition: 1s ease-in-out;

			}

			.dot {

				background-color: rgba(255, 255, 255, 0.7);
				border: solid 2px rgba(255, 255, 255, 0.5);

			}

			.LogoField {

				width: 500px;
				position: absolute;
				left: 90px;
				top: 1000px;
				-webkit-filter : drop-shadow(0px 3px 3px #C2E2D7);
				filter : drop-shadow(0px 3px 3px #C2E2D7); 
				z-index: 1;

			}

			.LabelShelter {

				color: black;
				font-family: autour_oneregular;
				font-size: 44px;
				text-decoration: underline;
				position: absolute;
				left: 470px;
				top: 1320px;
				z-index: 2;

			}

			.ContainerShelter {

				margin-top: 300px;
				margin-left: 30px;
				margin-right: 30px;


			}

			.ShelterKiri {

				width: 32%;
				float: left;
				text-align: left;

			}

			.ShelterKanan {

				width: 32%;
				float: right;
				text-align: left;

			}

			.ShelterTengah {

				width: 32%;
				display: inline-block;
				text-align: left;

			}

			.ImageShelter {

				width: 365px;
				box-shadow: 10px 10px 6px #000000CC;
				opacity: 1;

			}

			.HeaderShelter {

				font-family: acremedium;
				font-size: 28px;
				color: #FEBF56;

			}

			.ShelterLabel {

				font-family: acremedium;
				font-size: 26px;
				color: black;

			}

			.ShelterA {

				font-family: acremedium;
				font-size: 26px;
				color: #ABC7C9;
				text-decoration: none;

			}


			.ShelterA:hover {

				font-family: acremedium;
				font-size: 26px;
				color: #5DE5EE;
				text-decoration: none;

			}

			.BackgroundTengah {

				background-image: url("../Resources/images/104.jpg");
				background-position: center;
				background-repeat: no-repeat;
				background-size: cover;
				width : 100%;
				height: 100%;

			}

			.HeaderMaps {

				color: white;
				font-family: autour_oneregular;
				font-size: 44px;


			}

			.LabelMaps {

				color: white;
				font-family: autour_oneregular;
				font-size: 44px;


			}

			.Maps {

				width: 650px;
				height: 350px;
				border: 5px solid #FEBF56;
				border-radius: 40px;
				opacity: 1;

			}

			.dropbtn {

				border: none;
				font-family: acremedium;
				text-decoration: none;
				font-size: 33px;
				letter-spacing: 0;
				color: #E54A4A;
				opacity: 1;
				margin-right: 14px;
				background-color: Transparent;
    			background-repeat:no-repeat;
    			border: none;
    			cursor:pointer;

			}

			.dropdown {

				display: inline-block;
			}

			.dropdown-content {

				display: none;
				position: absolute;
				background-color: #f1f1f1;
				min-width: 160px;
				box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
				z-index: 1;
			}

			.dropdown-content a {

				color: black;
				font-size: 20px;
				letter-spacing: 0;
				padding: 12px 16px;
				text-decoration: none;
				display: block;
			}

			.dropdown-content a:hover {

				background-color: #ddd;

			}

			.dropdown:hover .dropdown-content {

				display: block;

			}

		</style>

	</head>

	<body>

		<div class = "BackgroundAtas">

			<div class = "ContainerNavbar">

				<div class = "NavbarKiri">

					<a href = "..\9. Home (Admin)\Home (Admin).php"> <img src = "../Resources/images/LOGOANML.png" class = "LogoUtama"> </a>

				</div>

				<div class = "NavbarKanan">

					<div style = "float: left;"> 

						<center>

							<a href = "..\10. Adopt A Pet (Admin)\Adoptapet(admin).php" class = "TulisanNavbar"> Adopt <br> a pet </a> 

						</center>

					</div>

					<div style = "float: left; margin-left: 85px;">

						<center>

							<a href = "..\11. Lost and Found (Admin)\Lostandfound(Admin).php" class = "TulisanNavbar"> Lost <br> and found </a>

						</center>

					</div>

					<div style = "float: left; margin-left: 85px; margin-top: 20px;">

						<center>

							<a href = "..\12. About us (Admin)\Aboutus (Admin).php" class = "TulisanNavbar"> About Us </a>

						</center>

					</div>

					<div style = "float: left; margin-left: 60px; margin-top: 20px;">

						<center>

							<div class="dropdown">

								<button class="dropbtn"> <?= $_SESSION["username"] ?> </button>

								<div class="dropdown-content">

									<a href = "..\18. Edit Account (Admin)\UpdateAccount (Admin).php"> Update Profil </a>
									<a href = "logout.php">Log Out</a>

								</div>

							</div>

						</center>

					</div>

				</div>

			</div>

			<center>

				<div class="slideshowContainer">


					<img class = "imageSlides" src = "../Resources/images/meong1.png" alt = "Cats With Text" style = "padding-right: 80px;">
					<img class = "imageSlides" src = "../Resources/images/meong2.png" alt = "Cats With Text" style = "margin-top: -650px;">
  
						<span id ="leftArrow" class="slideshowArrow">&#8249;</span>
						<span id ="rightArrow" class="slideshowArrow">&#8250;</span>
  
						<div class="slideshowCircles">

							<span class="circle dot"></span>
							<span class="circle"></span>

						</div>
  
				</div>

				<fieldset>

					<div>

						<img src = "../Resources/images/Untitled design (2).png" class = "LogoField">

					</div>

					<div>

						<label class = "LabelShelter"> <b> Shelter Spotlight </b> </label>

					</div>

					<div class = "ContainerShelter">

						<div class = "ShelterKiri">

							<div>

								<img src = "../Resources/images/shutterstock_224423782-1-e1527774744419.jpg" class = "ImageShelter">

							</div>

							<div style = "margin-top: 30px; border-left: 5px solid #CACACA; border-right: 5px solid #CACACA;">

								<div style = "margin-left: 20px;">

									<label class = "HeaderShelter"> <b> We Are Loving Our Animals </b> </label>

								</div>

								<div style = "margin-top: 20px; margin-left: 20px; margin-right: 20px;">

									<label class = "ShelterLabel"> <b> Our love to animals are just like loving to kids. We are care about their safety, hygiene, health, and of cource their need about love and care from human </b> </label>

								</div>

								<div style = "margin-top: 40px; margin-left: 20px;">

									<a href = "#" class = "ShelterA"> <b> Learn More  </b> </a>

								</div>

							</div>

						</div>

						<div class = "ShelterKanan">

							<div>

								<img src = "../Resources/images/cat-in-the-yard.jpg" class = "ImageShelter">

							</div>

							<div style = "margin-top: 30px; border-left: 5px solid #CACACA; border-right: 5px solid #CACACA;">

								<div style = "margin-left: 20px;">

									<label class = "HeaderShelter"> <b> Find Wandering Animals Near Your Home? </b> </label>

								</div>

								<div style = "margin-top: 20px; margin-left: 20px; margin-right: 20px;">

									<label class = "ShelterLabel"> <b> If you finding any wandering animals i near your house, just call our shelter, we will cature wandering animals to give them house and food </b> </label>

								</div>

								<div style = "margin-top: 40px; margin-left: 20px;">

									<a href = "#" class = "ShelterA"> <b> Learn More  </b> </a>

								</div>

							</div>

						</div>

						<div class = "ShelterTengah">

							<div>

								<img src = "../Resources/images/153558322-subtle-signs-of-a-sick-cat-632x475.jpg" class = "ImageShelter" style = "height: 244px;">

							</div>

							<div style = "margin-top: 30px; border-left: 5px solid #CACACA; border-right: 5px solid #CACACA; height: 399.8px;">

								<div style = "margin-left: 20px;">

									<label class = "HeaderShelter"> <b> Surrender With Your Pet? </b> </label>

								</div>

								<div style = "margin-top: 20px; margin-left: 20px; margin-right: 20px;">

									<label class = "ShelterLabel"> <b> We are always able to adopt your pet if you cant hold on anymore with your pet. Just come to our shelter </b> </label>

								</div>

								<div style = "margin-top: 40px; margin-left: 20px; margin-top: 145px;">

									<a href = "#" class = "ShelterA"> <b> Learn More  </b> </a>

								</div>

							</div>

						</div>
	
					</div>


				</fieldset>

		</div>

		<div class = "BackgroundTengah">

			<div style = "padding-top: 70px; padding-left: 80px;">

				<label class = "HeaderMaps"> <b> Shelter Location </b> </label>

			</div>

			<div style = "padding-top: 60px; padding-left: 80px;">

				<label class = "LabelMaps"> Our Location In : Jl. Nanggeleng - Cirahayu, Kujangsari, Kec. Bandung Kidul, Kota Bandung, Jawa Barat 40287 </label>

			</div>

			<div style = "padding-top: 10px; padding-left: 800px;">

				<div class = "Maps" id ="map"> </div>

			</div>

		</div>

		<div class>

			<div class = "bgFooter">

				<div style = "float: right;">

					<div style = "margin-right: 70px; margin-top: 50px;"> 

						<label class = "LabelLearn"> © Copyright 2019, powered by puppet </label> 

					</div>

				</div>

				<div style = "display: inline-block; margin-top: 380px; ">

					
					<div style = "margin-top: 20px; float: right; margin-left: 140px;"> 

						<a href = "#" class = "LabelMore"> <b> Learn More </b> </a> 

					</div>

					<div style = "margin-top: 20px; float: right; margin-left: 110px;"> 

						<a href = "#" class = "LabelMore"> <b> Our Shelter </b> </a> 

					</div>

					<div style = "margin-top: 20px; float: right; margin-left: 130px;"> 

						<a href = "#" class = "LabelMore"> <b> Contact Us </b> </a> 

					</div>

					<div style = "margin-top: 20px; float: right; margin-left: 250px;"> 

						<a href = "#" class = "LabelMore"> <b> About Us </b> </a> 

					</div>

				</div>

			</div>

		</div>


		<script>

			var imageSlides = document.getElementsByClassName('imageSlides');
			var circles = document.getElementsByClassName('circle');
			var leftArrow = document.getElementById('leftArrow');
			var rightArrow = document.getElementById('rightArrow');
			var counter = 0;

			function hideImages() {

				for (var i = 0; i < imageSlides.length; i++) {

				imageSlides[i].classList.remove('visible');

  				}

			}

			function removeDots() {

				for (var i = 0; i < imageSlides.length; i++) {

					circles[i].classList.remove('dot');

				}

			}

			function imageLoop() {

				var currentImage = imageSlides[counter];
				var currentDot = circles[counter];
				currentImage.classList.add('visible');
				removeDots();
				currentDot.classList.add('dot');
				counter++;

			}

			function arrowClick(e) {

				var target = e.target;

				if (target == leftArrow) {

					clearInterval(imageSlideshowInterval);
					hideImages();
					removeDots();

				if (counter == 1) {

					counter = (imageSlides.length - 1);
					imageLoop();
					imageSlideshowInterval = setInterval(slideshow, 10000);

				} else {

					counter--;
					counter--;
					imageLoop();
					imageSlideshowInterval = setInterval(slideshow, 10000);

				}

			} 

			else if (target == rightArrow) {

				clearInterval(imageSlideshowInterval);
				hideImages();
				removeDots();

				if (counter == imageSlides.length) {

					counter = 0;
					imageLoop();
					imageSlideshowInterval = setInterval(slideshow, 10000);

				} else {

					imageLoop();
					imageSlideshowInterval = setInterval(slideshow, 10000);

					}

				}

			}

			leftArrow.addEventListener('click', arrowClick);
			rightArrow.addEventListener('click', arrowClick);

			function slideshow() {

				if (counter < imageSlides.length) {
				hideImages();
				imageLoop();

			} else {

				counter = 0;
				hideImages();
				imageLoop();

				}
			}

			setTimeout(slideshow, 1000);
			var imageSlideshowInterval = setInterval(slideshow, 10000);

		</script>

		<script>

			function initMap() {

				var shelter = {lat: -6.965786, lng: 107.642581};

  				var map = new google.maps.Map(document.getElementById('map'), {zoom: 15, center: shelter});

				var marker = new google.maps.Marker({position: shelter, map: map});

			}

		</script>

		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBANwxbAjvKvNnn7g6sK0HedP8oyNDwInU&callback=initMap"> </script>

	</body>

</html>