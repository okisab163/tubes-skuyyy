<?php

	session_start();

	include("ConfigLocal.php");

?>

<html>

	<head>

		<title> Register Account </title>

		<style>


			* {

				margin : 0;
				padding : 0;
			}

			@font-face {


				font-family: "acremedium";

				src: url('../Resources/fonts/jonathan_ball_-_acre-medium-webfont.woff2') format('woff2'),

				url('../Resources/fonts/jonathan_ball_-_acre-medium-webfont.woff') format('woff');

				font-weight: normal;

				font-style: normal;

			}

			@font-face {
    

				font-family : "holtwood_one_scregular";

				src : url("../Resources/fonts/holtwoodonesc-webfont.woff2") format("woff2"),

				url("../Resources/fonts/holtwoodonesc-webfont.woff") format("woff");
				font-weight : normal;

				font-style : normal;
			
			}


			.BackgroundAtas {

				background-image: url("../Resources/images/Background.png");
				height: 150%;
				background-position: center;
				background-repeat: no-repeat;
				background-size: cover;
				position: relative;
				width : 100%;

			}

			.BackgroundBawah {

				background-image: url("../Resources/images/Background1.png");
				height: 50%;
				background-position: center;
				background-repeat: no-repeat;
				background-size: cover;
				position: relative;
				width : 100%;
				z-index: -1;

			}

			.LogoUtama {

				margin-top : 40px;
				margin-left : 50px;
				width : 150px;
				-webkit-filter : drop-shadow(0px 3px 3px #000000);
				filter : drop-shadow(0px 3px 3px #000000); 

			}

			fieldset {

				width : 700px;
				height: 1050px;
				background : #FFFFFF 0% 0% no-repeat padding-box;
				box-shadow : 10px 10px 6px #0000004F;
				border : 2px solid #FFA200;
				border-radius : 25px;
				opacity : 1;

			}

			.ContainerLogo {

				margin-top : 40px;
				margin-left : 200px;
				position : relative;
				width : 200px;
				height : 230px;

			}

			.ContainerLogoDepan {

				position: absolute;
				z-index: 2;
			}

			.ContainerLogoBelakang {

				position: absolute;
				z-index: 1;

			}

			.LogoKucing {

				width : 110px;
				-webkit-filter : drop-shadow(0px 3px 3px #000000);
				filter : drop-shadow(0px 3px 3px #000000);

			}

			.IconRegister {

				font-family : holtwood_one_scregular;
				letter-spacing : -3px;
				color : #4C9AE9;
				text-shadow : 0px 8px 4px #00000070;
				font-size : 58px;
				position : absolute;
				margin-top : 75px;
				left : -500px;
				width: 1000px;

			}

			::-webkit-input-value {

				color: #00000029;
				font-family : acremedium;
				font-size : 15px;

			}

			.ContainerName {

				width: 500px;
				height: 53px;

			}

			.NameKiri {

				float : left;
				width : 100px;

			}

			.NameKanan {

				float-right;
				margin-right: -260px;
				width : 220px;

			}

			.Firstname {

				width : 250px;
				height : 50px;
				box-shadow : 3px 3px 6px #00000029;
				border : 1px solid #00000029;
				border-radius : 15px;
				padding-left : 30px;
				font-family : acremedium;
				font-size : 15px;

			}

			.Firstname:focus {

				outline: none;

			}

			.LastName {

				width : 230px;
				height : 50px;
				box-shadow : 3px 3px 6px #00000029;
				border : 1px solid #00000029;
				border-radius : 15px;
				padding-left : 30px;
				font-family : acremedium;
				font-size : 15px;

			}

			.LastName:focus {

				outline: none;

			}

			.Username {

				margin-top : 5px;
				width : 500px;
				height : 50px;
				box-shadow : 3px 3px 6px #00000029;
				border : 1px solid #00000029;
				border-radius : 15px;
				padding-left : 30px;
				padding-top : 5px;
				font-family : acremedium;

			}

			.Username:focus {

				outline: none;

			}

			.Email {

				margin-top : 25px;
				width : 500px;
				height : 50px;
				box-shadow : 3px 3px 6px #00000029;
				border : 1px solid #00000029;
				border-radius : 15px;
				padding-left : 30px;
				padding-top : 5px;
				font-family : acremedium;

			}

			.Email:focus {

				outline: none;

			}

			.Address {

				margin-top : 25px;
				width : 500px;
				height : 50px;
				box-shadow : 3px 3px 6px #00000029;
				border : 1px solid #00000029;
				border-radius : 15px;
				padding-left : 30px;
				padding-top : 5px;
				font-family : acremedium;

			}

			.Address:focus {

				outline: none;

			}

			.PhoneNumber {

				margin-top : 25px;
				width : 500px;
				height : 50px;
				box-shadow : 3px 3px 6px #00000029;
				border : 1px solid #00000029;
				border-radius : 15px;
				padding-left : 30px;
				padding-top : 5px;
				font-family : acremedium;

			}

			.PhoneNumber:focus {

				outline: none;

			}

			.Password {

				margin-top : 25px;
				width : 500px;
				height : 50px;
				box-shadow : 3px 3px 6px #00000029;
				border : 1px solid #00000029;
				border-radius : 15px;
				padding-left : 30px;
				padding-top : 5px;

			}

			.Password:focus {

				outline: none;

			}

			.BirthDates {

				width : 500px;
				height : 50px;
				box-shadow : 3px 3px 6px #00000029;
				border : 1px solid #00000029;
				border-radius : 15px;
				padding-left : 30px;
				padding-top : 5px;

			}

			.BirthDates:focus {

				outline: none;

			}

			.LabelBirth {

				margin-top : 25px;
				margin-right: 360px;
				width : 100px;

			}

			.BirthDate {

				margin-top : 50px;
				font-family : acremedium;
				font-size : 15px;
				color: #00000029;

			}


			.ContainerDate {

				width: 500px;
				height: 53px;

			}

			.LabelGender {

				margin-top : 5px;
				margin-right: 360px;
				width : 100px;

			}

			.Gender {

				margin-top : 50px;
				font-family : acremedium;
				font-size : 15px;
				color: #00000029;

			}

			.ContainerGender {

				margin-top : 5px;
				width : 558px;
				height : 30px;

			}

			.GenderKiri {

				float : left;
				width : 150px; 

			}

			.GenderKanan {

				float : left;
				width : 150px; 
				left: 50px;

			}

			.Radio1 {

				transform: scale(3);
				width : 50px;
				cursor: pointer;

			}

			.Radio1:focus {

				outline: none;

			}

			.Radio2 {

				transform: scale(3);
				width : 50px;
				cursor: pointer;

			}

			.Radio2:focus {

				outline: none;

			}

			.Male {

				font-family : acremedium;
				font-size : 18px;
				color: #00000029;

			}

			.Female {

				font-family : acremedium;
				font-size : 18px;
				color: #00000029;

			}

			.SignIn {

				margin-top : 25px;
				width: 130px;
				height: 50px;
				background: #FF8000;
				box-shadow : 3px 3px 6px #00000073;
				border : 5px solid #FF8000;
				border-radius : 15px;
				position : relative;
				top : 3px;
				cursor: pointer;
				text-align : center;
				font-family : acremedium;
				font-size : 15px;
				color : #000000;

			}

			.SignIn:focus {

				outline: none;

			}

			.ContainerTerms {

				margin-top : 40px;

			}

			.Terms1 {

				font-family : acremedium;
				font-size : 26px;
				color : #000000;

			}

			.Terms2 {

				font-family : acremedium;
				font-size : 26px;
				color : #000000;

			}

			input[type=date]::-webkit-clear-button,
			input[type=date]::-webkit-inner-spin-button {

    			-webkit-appearance: none;
    			display: none;

			}

			input[type=date]::-webkit-calendar-picker-indicator {

  				width: 14px;
 				height: 18px;
				cursor: pointer;
				border-radius: 50%;
				margin-right: 8px;
			}

		</style>

	</head>

	<body>

		<div class = "BackgroundAtas">

			<a href = "..\9. Home (Admin)\Home (Admin).php"> <img src = "../Resources/images/LOGOANML.png" class = "LogoUtama"> </a>

			<center>

				<fieldset>

					<?php

						$username = $_SESSION["username"];

						$querydogs = mysqli_query($conn, "SELECT * FROM admin WHERE username = '$username'") or die(mysqli_error($conn));

 						while ($row = mysqli_fetch_assoc($querydogs)) {

					?>

					<form action = "commandUpdate.php" method = "POST">

						<div class = "ContainerLogo">

							<div class = "ContainerLogoDepan"> <img src = "../Resources/images/58a050065583a1291368eeb4.png" class = "LogoKucing"> </div>

							<div class = "ContainerLogoBelakang"> <label class = "IconRegister"> UPDATE ACCOUNT </label> </div>


						</div>

						<div class = "ContainerName">

							<div class = "NameKiri"> <input type = "text" class = "Firstname" value = "<?= $row['first_name'] ?>" name = "Firstname"> </div>

							<div class = "NameKanan"> <input type = "text" class = "LastName" value = "<?= $row['last_name'] ?>" name = "Lastname"> </div>

						</div>

							<br>

						<input type = "text" class = "Username" value = "<?= $_SESSION["username"] ?>" name = "Username">

							<br>

						<input type = "text" class = "Email" value = "<?= $row['email'] ?>" name = "Email">

							<br>

						<input type = "text" class = "Address" value = "<?= $row['address'] ?>" name = "Address">

							<br>

						<input type = "number" class = "PhoneNumber" value = "<?= $row['phonenumber'] ?>" name = "PhoneNumber">

							<br>

						<input type = "password" class = "Password" value = "<?= $row['password'] ?>" name = "Password">

							<br>

						<div class = "LabelBirth"> <label class = "BirthDate"> <b> Birth Date </b> </label> </div>

							<br>

						<div class = "ContainerDate">

							<input type = "date" class = "BirthDates " value = "<?= $row['birthdate'] ?>" name = "Date">

						</div>

							<br>

						<div class = "LabelGender"> <label class = "Gender"> <b> Gender </b> </label> </div>

							<br>

						<div class = "ContainerGender">

							<div class = "GenderKiri"> <input type = "radio" name = "Gender" <?=$row['gender']=="Male" ? "checked" : ""?> value = "Male" class = "Radio1"> <label class = "Male"> <b> Male </b> </label> </div>

							<div class = "GenderKanan"> <input type = "radio" name = "Gender" <?=$row['gender']=="Female" ? "checked" : ""?> value = "Female" class = "Radio2"> <label class = "Female"> <b> Female </b> </label> </div>

						</div>

							<br>

						<button class = "SignIn"  name = "SignUp"> <b> Update Account </b> </button>

							<br>

					</form>

					<?php

						}

					?>

				</fieldset>

			</center>

	</div>

	<div class = "BackgroundBawah">

	</div>

	</body>

</html>