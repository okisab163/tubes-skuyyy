-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 27, 2019 at 05:06 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `love_care`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(100) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phonenumber` bigint(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `first_name`, `last_name`, `username`, `email`, `address`, `phonenumber`, `password`, `birthdate`, `gender`) VALUES
(1, 'Oki', 'Fairuz', 'okisab163', 'okisabeni@gmail.com', 'l. H. Umayah II No.32, Citeureup Kamar 303, Dayeuhkolot', 82110730031, '082110730031', '1999-05-31', 'Male'),
(2, 'Ines', 'Putri', 'inesindah', 'ines.indahp@gmail.com', 'Jl. Umayah 2', 2918319, '082110730031', '1999-06-04', 'Female'),
(3, 'Adhi', 'Pradana', 'adhi', 'adhipradana099@gmail.com', 'Jl. Umayah 2', 239129, '12345', '2014-07-30', 'Male'),
(4, 'Ainunisa', 'Iqlima', 'Ainuns', 'ainun171725@gmail.com', 'Jl. Umayah 2', 812381231, '12345', '2014-06-11', 'Female'),
(5, 'Nurul', 'Farikhah', 'Nurul', 'nurulfarikhah386@gmail.com', 'Jl. Umayah 2', 82139123, '12345', '2014-07-16', 'Female');

-- --------------------------------------------------------

--
-- Table structure for table `pet_adopt_cat`
--

CREATE TABLE `pet_adopt_cat` (
  `id` int(100) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `datearrived` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `breed` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `age` int(100) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pet_adopt_cat`
--

INSERT INTO `pet_adopt_cat` (`id`, `image`, `datearrived`, `name`, `breed`, `sex`, `age`, `description`) VALUES
(1, '152166675-banfield-cat-pet-tips-632x475.jpg', '2019-10-29', 'Popopoy', 'Home Cat', 'Female', 2, '  wfdwefcewdcwedw '),
(2, '153558322-subtle-signs-of-a-sick-cat-632x475.jpg', '2019-11-28', 'Mamoy', 'Catsss', 'Male', 1, ' trhbdrfgvsfvsdevs'),
(3, 'cat-in-the-yard.jpg', '2019-11-29', 'dasa', 'capi', 'Male', 2, ' dewfewfcescersver');

-- --------------------------------------------------------

--
-- Table structure for table `pet_adopt_dog`
--

CREATE TABLE `pet_adopt_dog` (
  `id` int(100) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `datearrived` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `breed` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `age` int(100) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pet_adopt_dog`
--

INSERT INTO `pet_adopt_dog` (`id`, `image`, `datearrived`, `name`, `breed`, `sex`, `age`, `description`) VALUES
(1, 'anjing.jpg', '2019-11-04', 'sacasa', 'Puff', 'Female', 12, '  Puff Puff Puff Puff '),
(2, 'puppy-background-hd-1280x854-46542.jpg', '2019-11-12', 'Sabis', 'Pug', 'Female', 2, ' sacmowqdkoqwkdoq'),
(3, 'shutterstock_224423782-1-e1527774744419.jpg', '2019-11-28', 'Cacuy', 'Golden Retriever', 'Male', 3, ' adqwdqwdqwdq'),
(4, 'Funny-Dog-German-Wallpaper.jpg', '2019-11-18', 'dscs', 'Sheperd', 'Male', 3, ' dexmwkolcjweojmo'),
(5, 'Funny-Dog-Wallpaper-1-768x480.jpg', '2019-11-04', 'saca', 'Pug', 'Male', 1, ' mrulfliglk.gfi,fk'),
(6, 'Cute-Funny-Dog-Wallpaper-768x432.jpg', '2019-11-13', 'ewfw', 'gtgrf', 'Female', 3, ' ewgftrhtydrfverve'),
(7, 'Funny-Dog-Wallpaper-HD-768x432.jpg', '2019-11-28', 'safwe', 'wq2e1', 'Male', 2, ' saefcregrtgerfrcerc'),
(10, 'Free-Funny-Dog-Background-Download-768x480.jpg', '2019-11-19', 'Dhigidaw', 'adwq', 'Male', 2, ' Dawadawdawd aweuawue');

-- --------------------------------------------------------

--
-- Table structure for table `pet_adopt_other`
--

CREATE TABLE `pet_adopt_other` (
  `id` int(100) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `datearrived` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `breed` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `age` int(100) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pet_adopt_other`
--

INSERT INTO `pet_adopt_other` (`id`, `image`, `datearrived`, `name`, `breed`, `sex`, `age`, `description`) VALUES
(1, 'Cute-Animal-HD-Wallpaper_53.jpg', '2019-11-21', 'Nurul', 'monkey', 'Female', 2, '   sadcaecdwcwa  '),
(2, '793099.jpg', '2019-11-28', 'Mamay', 'marmut', 'Female', 1, ' rgberdgvesvgevg'),
(3, 'download-animals-grass-reptiles-tortoise-free-wallpapers-1920x1200.jpg', '2019-11-21', 'Turty', 'turtle', 'Female', 1, ' ewfcrgretfefe');

-- --------------------------------------------------------

--
-- Table structure for table `pet_lost_cat`
--

CREATE TABLE `pet_lost_cat` (
  `id` int(100) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `datearrived` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `breed` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `age` int(100) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pet_lost_cat`
--

INSERT INTO `pet_lost_cat` (`id`, `image`, `datearrived`, `name`, `breed`, `sex`, `age`, `description`) VALUES
(1, '82152bd5bfb1436e3d115f796fef2e4f.jpg', '2019-10-30', 'Mwee', 'Home Cat', 'Male', 3, ' weidcjwiejdqwoiejdoq'),
(2, 'tumblr_p48hqw9HfE1wo9q8wo4_500.png', '2019-11-22', 'Mwaaaaa', 'Home Cat', 'Female', 2, ' sdijcmiwejcioweji'),
(3, 'maxresdefault (3).jpg', '2019-11-20', 'Polite', 'Home Cat', 'Female', 2, ' ewdjxmdiwjeiweji'),
(4, '12d75fbc94fe1f1acc53f544b67b8a5dac563314r1-1074-1386v2_hq.jpg', '2019-11-20', 'gime', 'Home Cat', 'Male', 4, ' ewdjoiwejdiwjewqoijqw'),
(5, 'adorable-animal-baby-blur-177809.jpg', '2019-11-06', 'dakings', 'Home Cat', 'Male', 2, ' eidocjweoijwoijweoicjwoiecjiow');

-- --------------------------------------------------------

--
-- Table structure for table `pet_lost_dog`
--

CREATE TABLE `pet_lost_dog` (
  `id` int(100) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `datearrived` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `breed` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `age` int(100) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pet_lost_dog`
--

INSERT INTO `pet_lost_dog` (`id`, `image`, `datearrived`, `name`, `breed`, `sex`, `age`, `description`) VALUES
(1, '61625a1d9420e98f87c193e8b2ad918d.jpg', '2019-11-12', 'Salaman', 'Golden Retriever', 'Male', 2, ' ewfergerfefwefcwe'),
(2, 'adult-black-and-tan-hovawart-1144486.jpg', '2019-11-06', 'dennis', 'sadqwqdw', 'Male', 2, ' edwfdckwmedoldweik'),
(3, 'housetrain_adult_dog_hero.jpg', '2019-11-28', 'ewodjcowkow', 'eiwjfiw', 'Female', 5, ' ewdkowkdow'),
(4, '18-puppy-dog-eyes.w700.h467.jpg', '2019-11-20', 'sadasdt', 'dogs', 'Female', 2, '  ewfowejkdoweo ');

-- --------------------------------------------------------

--
-- Table structure for table `pet_lost_other`
--

CREATE TABLE `pet_lost_other` (
  `id` int(100) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `datearrived` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `breed` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `age` int(100) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pet_lost_other`
--

INSERT INTO `pet_lost_other` (`id`, `image`, `datearrived`, `name`, `breed`, `sex`, `age`, `description`) VALUES
(1, 'c55a037379006300ec2fed1012b516e9.jpg', '2019-11-20', 'aiiwq', 'Teuing', 'Male', 2, ' ewidjiwqjdiwejdiwejo'),
(2, 'xr8dsrxa7tu21.jpg', '2019-11-06', 'Baeb', 'Home Cat?', 'Male', 5, ' asjdqwhdqwdiqwhd'),
(3, 'maxresdefault.jpg', '2019-11-12', 'Dabes', 'racoon', 'Male', 4, ' ewidjciwejdiqwejdiw'),
(4, 'h6452894mkh21.jpg', '2019-11-06', 'Naon Ieu', 'Teuing Ah', 'Male', 3, ' ewjcdiwjediwjeicjwei'),
(5, 'maxresdefault (2).jpg', '2019-11-06', 'Named', 'racoon', 'Male', 2, ' weijdciwejciw'),
(6, 'black-chimpanzee-smiling-50582.jpg', '2019-11-12', 'adhi', 'monkey', 'Male', 2, ' ewciwjdoiwjeo'),
(7, 'zoo-bear-35435.jpg', '2019-11-05', 'babybear', 'bear', 'Female', 5, ' ewjidcwoejcweoicjweio');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(100) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phonenumber` bigint(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `email`, `address`, `phonenumber`, `password`, `birthdate`, `gender`) VALUES
(1, 'Paijo', 'Palahak', 'paihak', 'paihaku@gmail.com', 'Disini Senang', 921849201, '123', '1986-03-11', 'Male'),
(2, 'Adhi', 'Pradana', 'adhiprdna', 'adhipradana099@gmail.com', 'Bandung', 2147483647, 'Triple6', '1999-06-06', 'Male');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pet_adopt_cat`
--
ALTER TABLE `pet_adopt_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pet_adopt_dog`
--
ALTER TABLE `pet_adopt_dog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pet_adopt_other`
--
ALTER TABLE `pet_adopt_other`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pet_lost_cat`
--
ALTER TABLE `pet_lost_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pet_lost_dog`
--
ALTER TABLE `pet_lost_dog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pet_lost_other`
--
ALTER TABLE `pet_lost_other`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pet_adopt_cat`
--
ALTER TABLE `pet_adopt_cat`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pet_adopt_dog`
--
ALTER TABLE `pet_adopt_dog`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `pet_adopt_other`
--
ALTER TABLE `pet_adopt_other`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pet_lost_cat`
--
ALTER TABLE `pet_lost_cat`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pet_lost_dog`
--
ALTER TABLE `pet_lost_dog`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pet_lost_other`
--
ALTER TABLE `pet_lost_other`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
