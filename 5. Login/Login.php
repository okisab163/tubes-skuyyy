<?php

session_start();
?>

<html>

	<head>
	
		<title> Login </title>

		<style>

			* {

				margin : 0;
				padding : 0;
			}

			@font-face {

				font-family: "acremedium";

				src: url('../Resources/fonts/jonathan_ball_-_acre-medium-webfont.woff2') format('woff2'),

				url('../Resources/fonts/jonathan_ball_-_acre-medium-webfont.woff') format('woff');

				font-weight: normal;

				font-style: normal;

			}

			@font-face {

				font-family : "holtwood_one_scregular";

				src : url("../Resources/fonts/holtwoodonesc-webfont.woff2") format("woff2"),

				url("../Resources/fonts/holtwoodonesc-webfont.woff") format("woff");
				font-weight : normal;

				font-style : normal;

			}

			html { 

				background : url(../Resources/images/Violet+Simple+Class+Schedule_upscaled_illustration_x4.png) no-repeat center center; 
				-webkit-background-size : contain;
				-moz-background-size : contain;
				-o-background-size : contain;
				background-size : 1920px;

			}

			.LogoUtama {

				margin-top : 40px;
				margin-left : 50px;
				width : 150px;
				-webkit-filter : drop-shadow(0px 3px 3px #000000);
				filter : drop-shadow(0px 3px 3px #000000); 

			}

			fieldset {

				width : 700px;
				height: 600px;
				background : #FFFFFF 0% 0% no-repeat padding-box;
				box-shadow : 10px 10px 6px #0000004F;
				border : 2px solid #FFA200;
				border-radius : 25px;
				opacity : 1;

			}

			.ContainerLogo {

				margin-top : 40px;
				margin-left : 200px;
				position : relative;
				width : 200px;
				height : 230px;

			}

			.ContainerLogoDepan {

				position: absolute;
				z-index: 2;
			}

			.ContainerLogoBelakang {

				position: absolute;
				z-index: 1;
			}

			.LogoKucing {

				width : 110px;
				-webkit-filter : drop-shadow(0px 3px 3px #000000);
				filter : drop-shadow(0px 3px 3px #000000);

			}

			.IconRegister {

				font-family : holtwood_one_scregular;
				letter-spacing : -3px;
				color : #4C9AE9;
				text-shadow : 0px 8px 4px #00000070;
				font-size : 60px;
				position : absolute;
				margin-top : 75px;
				right : -104px;

			}

			::-webkit-input-placeholder {

				color: #00000029;
				font-family : acremedium;
				font-size : 15px;

			}

			.Username {

				width : 500px;
				height : 50px;
				box-shadow : 3px 3px 6px #00000029;
				border : 1px solid #00000029;
				border-radius : 15px;
				padding-left : 30px;
				font-family : acremedium;
				font-size : 15px;


			}

			.Username:focus {

				outline: none;

			}

			.Password {

				margin-top : 25px;
				width : 500px;
				height : 50px;
				box-shadow : 3px 3px 6px #00000029;
				border : 1px solid #00000029;
				border-radius : 15px;
				padding-left : 30px;
				padding-top : 5px;

			}

			.Password:focus {

				outline: none;

			}

			.DivParentSignIn {

				width: 500px;
				height: 80px;

			}

			.DivKiriSignIn {

				float : left;
				margin-left : 70px;
				width : 100px;

			}

			.DivKananSignIn {

				float-right;
				margin-left : 150px;
				width : 220px;

			}

			.SignIn {

				margin-top : 25px;
				width: 130px;
				height: 50px;
				background: #FF8000;
				box-shadow : 3px 3px 6px #00000073;
				border : 5px solid #FF8000;
				border-radius : 15px;
				position : relative;
				top : 3px;
				cursor: pointer;
				text-align : center;
				font-family : acremedium;
				font-size : 15px;
				color : #000000;

			}

			.SignIn:focus {

				outline: none;

			}

			.CheckBox {

				margin-top : 40px;
				width : 25px;
				height : 25px;
				background : white;
				border-radius :5px;
				border : 2px solid #555;
				vertical-align: bottom;
				position : relative;
				top : 2px;

			}

			.RememberAccount {

				font-family : acremedium;
				font-size : 15px;
				color: #000000;
				
			}

			.TeksBiru {

				margin-top : 20px;
				width : 500px;

			}

			.ForgetPassword {

				text-align : center;
				text-decoration : underline;
				font-family : acremedium;
				font-size : 15px;
				color: #34A6FF;
			}

			.CreateAccount {

				text-align : center;
				text-decoration : underline;
				font-family : acremedium;
				font-size : 15px;
				color: #34A6FF;

			}

		</style>

	</head>

	<body>

		<a href = "..\1. Home\Home.php"> <img src = "../Resources/images/LOGOANML.png" class = "LogoUtama"> </a>

		<center>

			<fieldset>

				<form action = "CommandLogin.php" method = "POST">

					<div class = "ContainerLogo">

						<div class = "ContainerLogoDepan"> <img src = "../Resources/images/58a050065583a1291368eeb4.png" class = "LogoKucing"> </div>

						<div class = "ContainerLogoBelakang"> <label class = "IconRegister"> LOGIN </label> </div>


					</div>

					<input type = "text" class = "Username" placeholder = "Username" name = "Username">

						<br>

					<input type = "password" class = "Password" placeholder = "Password" name = "Password">

						<br>

					<div class = "DivParentSignIn">

						<div class = "DivKiriSignIn"> <button class = "SignIn" name = "SignIn"> Sign In </button> </div>

						<div class = "DivKananSignIn"> <input type = "checkbox" class = "CheckBox"> <label class = "RememberAccount"> &#160 Remember My Account </label> </div>

					</div>
						<br>

					<div class = "TeksBiru">

					<div style = "float: left; margin-left: 120px;">

						<a href = "..\7. Forgotpassword\ForgotPassword.php" class = "ForgetPassword"> <b> Forget Password? </b> </a>

					</div>

					<div style = "float: right; margin-right: 120px;">

						<a href = "..\6. Registeraccount\RegisterAccount.php" class = "CreateAccount"> <b> Create Account </b> </a>

					</div>

					</div>

				</form>

			</fieldset>

		</center>

			<br>
			<br>
			<br>
			<br>

	</body>

</html>