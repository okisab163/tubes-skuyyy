<?php

	session_start();
	include("ConfigLocal.php");

?>

<html>

	<head>
	
		<title> Adopt A Pet </title>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

		<style>

			* {

				margin : 0;
				padding : 0;
			}

			@font-face {

				font-family: "acremedium";

				src: url('../Resources/fonts/jonathan_ball_-_acre-medium-webfont.woff2') format('woff2'),

				url('../Resources/fonts/jonathan_ball_-_acre-medium-webfont.woff') format('woff');

				font-weight: normal;

				font-style: normal;

			}

			@font-face {

				font-family : "holtwood_one_scregular";

				src : url("../Resources/fonts/holtwoodonesc-webfont.woff2") format("woff2"),

				url("../Resources/fonts/holtwoodonesc-webfont.woff") format("woff");
				font-weight : normal;

				font-style : normal;

			}

			@font-face {

				font-family: 'autour_oneregular';
				src: url('../Resources/fonts/autourone-regular-webfont.woff2') format('woff2'),
				url('../Resources/fonts/autourone-regular-webfont.woff') format('woff');
				font-weight: normal;
				font-style: normal;

			}

			@font-face {

				font-family: 'primerprint';
				src: url('../Resources/fonts/primer_print-webfont.woff2') format('woff2'),
				url('../Resources/fonts/primer_print-webfont.woff') format('woff');
				font-weight: normal;
				font-style: normal;

			}

			.BackgroundAtas {

				background-image: url("../Resources/images/Violet+Simple+Class+Schedule_upscaled_illustration_x4.png");
				height: 292%;
				background-position: center;
				background-repeat: no-repeat;
				background-size: cover;
				position: relative;
				width : 100%;

			}

			.ContainerNavbar {

				height: 10px;

			}

			.NavbarKiri {

				float: left;

			}

			.NavbarKanan {

				margin-top: 40px;
				width: 800px;
				float: right;

			}

			.TulisanNavbar {

				font-family: acremedium;
				text-decoration: none;
				font-size: 33px;
				letter-spacing: 0;
				color: #E54A4A;
				opacity: 1;

			}

			.TulisanNavbar:hover {

				font-family: acremedium;
				text-decoration: none;
				font-size: 33px;
				letter-spacing: 0;
				color: #F34D14;
				opacity: 1;

			}

			.TulisanNavbarTerpilih {

				font-family: acremedium;
				text-decoration: none;
				font-size: 33px;
				letter-spacing: 0;
				color: #4AA7E5;
				opacity: 1;

			}

			.LogoUtama {

				margin-top : 40px;
				margin-left : 50px;
				width : 150px;
				-webkit-filter : drop-shadow(0px 3px 3px #000000);
				filter : drop-shadow(0px 3px 3px #000000); 

			}

			fieldset {

				width : 1200px;
				height: 500px;
				margin-top: 440px;
				background : white;
				box-shadow : 10px 10px 6px #0000004F;
				border : 2px solid #FFA200;
				border-radius : 25px;
				opacity : 1;

			}

			.ContainerLogo {

				margin-top : 40px;
				margin-left : 200px;
				position : relative;
				width : 200px;
				height : 230px;
				position: absolute;
				top: 210px;
				right: 100px;

			}

			.ContainerLogoDepan {

				position: absolute;
				z-index: 2;
			}

			.ContainerLogoBelakang {

				position: absolute;
				z-index: 1;
				left: -500px;
				width: 500px;
			}

			.LogoKucing {

				width : 110px;
				-webkit-filter : drop-shadow(0px 3px 3px #000000);
				filter : drop-shadow(0px 3px 3px #000000);

			}

			.IconRegister {

				font-family : holtwood_one_scregular;
				letter-spacing : -3px;
				color : #4C9AE9;
				text-shadow : 0px 8px 4px #00000070;
				font-size : 60px;
				position : absolute;
				margin-top : 75px;
				right : -104px;

			}

			::-webkit-input-placeholder {

				color: #00000029;
				font-family : acremedium;
				font-size : 15px;

			}

			.bgFooter {

				margin-top: 1400px;
 				background-image: url("../Resources/images/footer.jpg");
				background-repeat: no-repeat;
				background-size: 100% 100%;
				width : 100%;
				height: 550px;
			}
	
			.LabelLearn {

				color: #FEBF56;
				font-family: primerprint;
				font-size: 15px;

			}

			.LabelMore {

				color: black;
				font-family: primerprint;
				font-size: 42px;
				text-decoration: none;

			}

			.LabelMore:hover {

				color: #FFAA00;
				font-family: primerprint;
				font-size: 42px;
				text-decoration: none;

			}

			.LabelLove {

				color: black;
				font-family: autour_oneregular;
				font-size: 44px;

			}

			.ButtonAdopt {

				width: 300px;
				height: 70px;
				margin-top: 30px;
				margin-right:100px;
				float: right;
				background-color: #EB7D00;
				box-shadow: 3px 3px 6px #00000073;
				border: 5px solid #FF8000;
				border-radius: 14px;
				cursor: pointer;
				opacity: 1;
				font-family : acremedium;
				font-size : 24px;
				color: black;

			}

			.ContainerAdopt {

				margin-top: 50px;

			}

			.AdoptKiri {

				float: left;

			}

			.AdoptKanan {

				float: right;
				width: 700px;
				margin-top: 90px;
				margin-right: 40px;

			}

			.GambarAdopt {

				width: 400px;
				margin-left: 30px;

			}

			.AdoptHeader {

				font-family : acremedium;
				font-size : 45px;
				color: #4AA7E5;


			}

			.AdoptLabel {

				margin-top: 10px;
				font-family : acremedium;
				font-size : 28px;
				color: #66747E;

			}

			.ContainerAnimals {

				width : 1200px;
				height: 2600px;
				margin-top: 90px;
				background : white;
				box-shadow : 10px 10px 6px #0000004F;
				border : 2px solid #FFA200;
				border-radius : 25px;
				opacity : 1;

			}

			.AnimalsLabel {

				font-family : acremedium;
				font-size : 45px;
				color: #4AA7E5;

			}

			.ImageAnimals {

				width: 300px;
				height: 200px;
				box-shadow: 10px 10px 6px #000000CC;
				border-radius: 50px;
				opacity: 1;

			}

			.TulisanForm {

				font-family : acremedium;
				font-size : 20px;
				color: black;

			}

			.image-upload>input {

				display: none;

			}

			.image-upload {

				-webkit-filter : drop-shadow(0px 3px 3px #000000);
				filter : drop-shadow(0px 3px 3px #000000); 

			}

			.scroll::-webkit-scrollbar {

				width: 12px;
			}

			.scroll::-webkit-scrollbar-track {

				-webkit-box-shadow: inset 0 0 6px black; 
				border-radius: 10px;

			}

			.scroll::-webkit-scrollbar-thumb {

				border-radius: 10px;
				-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5); 

			}

			.dropbtn {

				border: none;
				font-family: acremedium;
				text-decoration: none;
				font-size: 33px;
				letter-spacing: 0;
				color: #E54A4A;
				opacity: 1;
				margin-right: 14px;
				background-color: Transparent;
    			background-repeat:no-repeat;
    			border: none;
    			cursor:pointer;

			}

			.dropdown {

				display: inline-block;
			}

			.dropdown-content {

				display: none;
				position: absolute;
				background-color: #f1f1f1;
				min-width: 160px;
				box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
				z-index: 1;
			}

			.dropdown-content a {

				color: black;
				font-size: 20px;
				letter-spacing: 0;
				padding: 12px 16px;
				text-decoration: none;
				display: block;
			}

			.dropdown-content a:hover {

				background-color: #ddd;

			}

			.dropdown:hover .dropdown-content {

				display: block;

			}

		</style>

	</head>

	<body>

		<div class = "BackgroundAtas">

			<div class = "ContainerNavbar">

				<div class = "NavbarKiri">

					<a href = "..\13. Home (User)\Home (User).php"> <img src = "../Resources/images/LOGOANML.png" class = "LogoUtama"> </a>

				</div>

				<div class = "NavbarKanan">

					<div style = "float: left;"> 

						<center>

							<a href = "Adoptapet (User).php" class = "TulisanNavbarTerpilih"> Adopt <br> a pet </a> 

						</center>

					</div>

					<div style = "float: left; margin-left: 85px;">

						<center>

							<a href = "..\16. Lost and Found (User)\Lostandfound (User).php" class = "TulisanNavbar"> Lost <br> and found </a>

						</center>

					</div>

					<div style = "float: left; margin-left: 85px; margin-top: 20px;">

						<center>

							<a href = "..\17. About us (User)\Aboutus (User).php" class = "TulisanNavbar"> About Us </a>

						</center>

					</div>

					<div style = "float: left; margin-left: 60px; margin-top: 20px;">

						<center>

							<div class="dropdown">

								<button class="dropbtn"> <?= $_SESSION["username"] ?> </button>

								<div class="dropdown-content">

									<a href = "..\19. Edit Account (User)\UpdateAccount (User).php"> Update Profil </a>
									<a href = "logout.php">Log Out</a>

								</div>

							</div>

						</center>

					</div>

				</div>

			</div>

			<center>

				<div class = "ContainerLogo">

					<div class = "ContainerLogoDepan"> <img src = "../Resources/images/58a050065583a1291368eeb4.png" class = "LogoKucing"> </div>

					<div class = "ContainerLogoBelakang"> <label class = "IconRegister"> ADOPT A PET </label> </div>


				</div>

				<fieldset>

					<div class = "ContainerAdopt">

						<div class = "AdoptKiri">

							<img src = "../Resources/images/Untitled design (1).png" class = "GambarAdopt">

						</div>

						<div class = "AdoptKanan">

							<div>

								<label class = "AdoptHeader"> Adopt A Pet </label>

							</div>

							<div style = "margin-top: 30px;">

								<label class = "AdoptLabel"> Are you thinking about welcoming a new pet in your home? Here are some great animals you can adopt from the Love & Care Animals Shelter </label>

							</div>

						</div>

					</div>

					<hr style = "border: 1.5px solid #AEB7BC; opacity: 1; width : 90%;">

				</fieldset>

				<div class = "ContainerAnimals">

					<div style = "text-align: left; margin-left: 50px; margin-top: 40px;">

						<label class = "AnimalsLabel"> Dogs </label>

					</div>

					<div style = "margin-top: 50px;">

						<hr style = "border: 1.5px solid #AEB7BC; opacity: 1; width : 95%; margin: 0em auto;">

					</div>

					<div class = "scroll" style = "width: 100%; height: 25%; overflow: auto; padding-bottom: 50px;">

					<?php

						$querydogs = mysqli_query($conn, "SELECT * FROM pet_adopt_dog") or die(mysqli_error($conn));

 						while ($row = mysqli_fetch_assoc($querydogs)) {

					?>

					<div>

						<div style = "float: left; margin-top: 50px; margin-left: 70px; width: 300px; height: 200px;box-shadow: 10px 10px 6px #000000CC; border-radius: 50px; background-image: url('../Server Uploads/Adopt Dog/<?= $row['image']; ?>'); background-repeat: no-repeat; background-size: 100% 100%;">

							<div style = "width: 100%;">

								<button type = "button" style = "border: none; background: none; cursor: pointer; width: 100%; height: 100%; border-radius: 50px; opacity: 0;" class = "ButtonEdit" name = "ButtonEdit" data-toggle="modal" data-target="#ModalEditDog-<?= $row['id'] ?>">Ini Button Ya </button>

								<form action = "..\15. Adopt A Pet (1) (User)\Adoptapet(1) (User).php" method = "POST" enctype = "multipart/form-data">

									<input type = "text" name = "Id" value = "<?= $row['id'] ?>" hidden>
									<input type = "text" name = "Image" value = "<?= $row['image'] ?>" hidden>

									<div class="modal fade" id="ModalEditDog-<?= $row['id'] ?>" role="dialog">

    									<div class="modal-dialog">
    
      										<div class="modal-content">

        										<div class="modal-header">

          											<button type="button" class="close" data-dismiss="modal">&times;</button>
          											<h4 class="modal-title"> Update Animal </h4>

        										</div>

        										<div class="modal-body">

        											<div>

														<div style = "margin-left: 35px;" class="imageupload">

														<center>

															<label>

																<img id = "previewimages" style = "width: 400px;" src = "../Server Uploads/Adopt Dog/<?= $row['image']; ?>">

															</label>

														</center>

														</div>


        											</div>

        											<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Date Arrived </label> </div>

            											<div style = "float: left; margin-left: 43px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "date" name = "DateArrivedUpdate" value = "<?= $row['datearrived'] ?>" required> </div>

           											</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Name </label> </div>

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "text" name = "AnimalNameUpdate" value = "<?= $row['name'] ?>" required> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Breed </label> </div>

            											<div style = "float: left; margin-left: 105px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "text" name = "BreedUpdate" value = "<?= $row['breed'] ?>" required> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Sex </label> </div>

            											<div style = "float: left; margin-left: 125px;"> <label class = "TulisanForm"> : </label> </div>

														<div style = "float: left; margin-left: 40px;"> <input type = "radio" name = "AnimalSexUpdate" <?=$row['sex']=="Male" ? "checked" : ""?> value = "Male"> <label class = "TulisanForm">  Male </label> </div>

            											<div style = "float: left; margin-left: 40px;"> <input type = "radio" name = "AnimalSexUpdate" <?=$row['sex']=="Female" ? "checked" : ""?> value = "Female"> <label class = "TulisanForm"> Female </label> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Age </label> </div>

            											<div style = "float: left; margin-left: 120px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "number" name = "AnimalAgeUpdate" value = "<?= $row['age'] ?>" required> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Description </label> </div>

            											<div style = "float: left; margin-left: 55px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <textarea name = "DescriptionUpdate" required> <?= $row['description'] ?> </textarea> </div>

       												</div>

												</div>

												<div class="modal-footer">

													<button type="submit" class="btn btn-default" name = "Adopt"> Adopt </button>

												</div>

      										</div>
      
    									</div>

									</div>

								</form>

							</div>

						</div>

					</div>

					<?php

						}

					?>

					</div>

					<div>

						<hr style = "border: 1.5px solid #AEB7BC; opacity: 1; width : 95%; margin: 0em auto;">

					</div>

					<div style = "text-align: left; margin-left: 50px; margin-top: 40px;">

						<label class = "AnimalsLabel"> Cats </label>

					</div>

					<div style = "margin-top: 50px;">

						<hr style = "border: 1.5px solid #AEB7BC; opacity: 1; width : 95%; margin: 0em auto;">

					</div>

					<div class = "scroll" style = "width: 100%; height: 25%; overflow: auto; padding-bottom: 50px;">

					<?php

						$querycats = mysqli_query($conn, "SELECT * FROM pet_adopt_cat") or die(mysqli_error($conn));

 						while ($row = mysqli_fetch_assoc($querycats)) {

					?>

					<div>

						<div style = "float: left; margin-top: 50px; margin-left: 70px; width: 300px; height: 200px;box-shadow: 10px 10px 6px #000000CC; border-radius: 50px; background-image: url('../Server Uploads/Adopt Cat/<?= $row['image']; ?>'); background-repeat: no-repeat; background-size: 100% 100%;">

							<div style = "width: 100%;">

								<button type = "button" style = "border: none; background: none; cursor: pointer; width: 100%; height: 100%; border-radius: 50px; opacity: 0;" class = "ButtonEdit" name = "ButtonEdit" data-toggle="modal" data-target="#ModalEditCat-<?= $row['id'] ?>"> Ini Button </button>

								<form action = "..\15. Adopt A Pet (1) (User)\Adoptapet(1) (User).php" method = "POST" enctype = "multipart/form-data">

									<input type = "text" name = "Id" value = "<?= $row['id'] ?>" hidden>
									<input type = "text" name = "Image" value = "<?= $row['image'] ?>" hidden>

									<div class="modal fade" id="ModalEditCat-<?= $row['id'] ?>" role="dialog">

    									<div class="modal-dialog">
    
      										<div class="modal-content">

        										<div class="modal-header">

          											<button type="button" class="close" data-dismiss="modal">&times;</button>
          											<h4 class="modal-title"> Update Animal </h4>

        										</div>

        										<div class="modal-body">

        											<div>

														<div style = "margin-left: 35px;" class="imageupload">

														<center>

															<label>

																<img id = "previewimages" style = "width: 400px;" src = "../Server Uploads/Adopt Cat/<?= $row['image']; ?>">

															</label>

														</center>

														</div>


        											</div>

        											<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Date Arrived </label> </div>

            											<div style = "float: left; margin-left: 43px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "date" name = "DateArrivedUpdate" value = "<?= $row['datearrived'] ?>" required> </div>

           											</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Name </label> </div>

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "text" name = "AnimalNameUpdate" value = "<?= $row['name'] ?>" required> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Breed </label> </div>

            											<div style = "float: left; margin-left: 105px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "text" name = "BreedUpdate" value = "<?= $row['breed'] ?>" required> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Sex </label> </div>

            											<div style = "float: left; margin-left: 125px;"> <label class = "TulisanForm"> : </label> </div>

														<div style = "float: left; margin-left: 40px;"> <input type = "radio" name = "AnimalSexUpdate" <?=$row['sex']=="Male" ? "checked" : ""?> value = "Male"> <label class = "TulisanForm">  Male </label> </div>

            											<div style = "float: left; margin-left: 40px;"> <input type = "radio" name = "AnimalSexUpdate" <?=$row['sex']=="Female" ? "checked" : ""?> value = "Female"> <label class = "TulisanForm"> Female </label> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Age </label> </div>

            											<div style = "float: left; margin-left: 120px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "number" name = "AnimalAgeUpdate" value = "<?= $row['age'] ?>" required> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Description </label> </div>

            											<div style = "float: left; margin-left: 55px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <textarea name = "DescriptionUpdate" required> <?= $row['description'] ?> </textarea> </div>

       												</div>

												</div>

												<div class="modal-footer">

													<button type="submit" class="btn btn-default" name = "Adopt"> Adopt </button>

												</div>

      										</div>
      
    									</div>

									</div>

								</form>

							</div>

						</div>

					</div>

					<?php

						}

					?>

					</div>

					<div>

						<hr style = "border: 1.5px solid #AEB7BC; opacity: 1; width : 95%; margin: 0em auto;">

					</div>

					<div style = "text-align: left; margin-left: 50px; margin-top: 40px;">

						<label class = "AnimalsLabel"> Other Animals </label>

					</div>

					<div style = "margin-top: 50px;">

						<hr style = "border: 1.5px solid #AEB7BC; opacity: 1; width : 95%; margin: 0em auto;">

					</div>

					<div class = "scroll" style = "width: 100%; height: 25%; overflow: auto; padding-bottom: 50px;">

					<?php

						$queryother = mysqli_query($conn, "SELECT * FROM pet_adopt_other") or die(mysqli_error($conn));

 						while ($row = mysqli_fetch_assoc($queryother)) {

					?>

					<div>

						<div style = "float: left; margin-top: 50px; margin-left: 70px; width: 300px; height: 200px;box-shadow: 10px 10px 6px #000000CC; border-radius: 50px; background-image: url('../Server Uploads/Adopt Other/<?= $row['image']; ?>'); background-repeat: no-repeat; background-size: 100% 100%;">

							<div style = "width: 100%;">

								<button type = "button" style = "border: none; background: none; cursor: pointer; width: 100%; height: 100%; border-radius: 50px; opacity: 0;" class = "ButtonEdit" name = "ButtonEdit" data-toggle="modal" data-target="#ModalEditOther-<?= $row['id'] ?>"> Ini Button </button>

								<form action = "..\15. Adopt A Pet (1) (User)\Adoptapet(1) (User).php" method = "POST" enctype = "multipart/form-data">

									<input type = "text" name = "Id" value = "<?= $row['id'] ?>" hidden>
									<input type = "text" name = "Image" value = "<?= $row['image'] ?>" hidden>

									<div class="modal fade" id="ModalEditOther-<?= $row['id'] ?>" role="dialog">

    									<div class="modal-dialog">
    
      										<div class="modal-content">

        										<div class="modal-header">

          											<button type="button" class="close" data-dismiss="modal">&times;</button>
          											<h4 class="modal-title"> Update Animal </h4>

        										</div>

        										<div class="modal-body">

        											<div>

														<div style = "margin-left: 35px;" class="imageupload">

														<center>

															<label>

																<img id = "previewimages" style = "width: 400px;" src = "../Server Uploads/Adopt Other/<?= $row['image']; ?>">

															</label>

														</center>

														</div>


        											</div>

        											<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Date Arrived </label> </div>

            											<div style = "float: left; margin-left: 43px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "date" name = "DateArrivedUpdate" value = "<?= $row['datearrived'] ?>" required> </div>

           											</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Name </label> </div>

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "text" name = "AnimalNameUpdate" value = "<?= $row['name'] ?>" required> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Breed </label> </div>

            											<div style = "float: left; margin-left: 105px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "text" name = "BreedUpdate" value = "<?= $row['breed'] ?>" required> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Sex </label> </div>

            											<div style = "float: left; margin-left: 125px;"> <label class = "TulisanForm"> : </label> </div>

														<div style = "float: left; margin-left: 40px;"> <input type = "radio" name = "AnimalSexUpdate" <?=$row['sex']=="Male" ? "checked" : ""?> value = "Male"> <label class = "TulisanForm">  Male </label> </div>

            											<div style = "float: left; margin-left: 40px;"> <input type = "radio" name = "AnimalSexUpdate" <?=$row['sex']=="Female" ? "checked" : ""?> value = "Female"> <label class = "TulisanForm"> Female </label> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Age </label> </div>

            											<div style = "float: left; margin-left: 120px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "number" name = "AnimalAgeUpdate" value = "<?= $row['age'] ?>" required> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Description </label> </div>

            											<div style = "float: left; margin-left: 55px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <textarea name = "DescriptionUpdate" required> <?= $row['description'] ?> </textarea> </div>

       												</div>

												</div>

												<div class="modal-footer">

													<button type="submit" class="btn btn-default" name = "Adopt"> Adopt </button>

												</div>

      										</div>
      
    									</div>

									</div>

								</form>

							</div>

						</div>

					</div>

					<?php

						}

					?>

					</div>

					<div>

						<hr style = "border: 1.5px solid #AEB7BC; opacity: 1; width : 95%; margin: 0em auto;">

					</div>

					<br>
					<br>
					<br>

				</div>

		</div>

		<div class>

			<div class = "bgFooter">

				<div style = "float: right;">

					<div style = "margin-right: 70px; margin-top: 50px;"> 

						<label class = "LabelLearn"> © Copyright 2019, powered by puppet </label> 

					</div>

				</div>

				<div style = "display: inline-block; margin-top: 380px; ">

					
					<div style = "margin-top: 20px; float: right; margin-left: 140px;"> 

						<a href = "#" class = "LabelMore"> <b> Learn More </b> </a> 

					</div>

					<div style = "margin-top: 20px; float: right; margin-left: 110px;"> 

						<a href = "#" class = "LabelMore"> <b> Our Shelter </b> </a> 

					</div>

					<div style = "margin-top: 20px; float: right; margin-left: 130px;"> 

						<a href = "#" class = "LabelMore"> <b> Contact Us </b> </a> 

					</div>

					<div style = "margin-top: 20px; float: right; margin-left: 250px;"> 

						<a href = "#" class = "LabelMore"> <b> About Us </b> </a> 

					</div>

				</div>

			</div>

		</div>
	</body>

</html>