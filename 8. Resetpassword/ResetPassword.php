<?php

	session_start();

?>

<html>

	<head>
	
		<title> Forgot Password </title>

		<style>

			* {

				margin : 0;
				padding : 0;
			}

			@font-face {


				font-family: "acremedium";

				src: url('../Resources/fonts/jonathan_ball_-_acre-medium-webfont.woff2') format('woff2'),

				url('../Resources/fonts/jonathan_ball_-_acre-medium-webfont.woff') format('woff');

				font-weight: normal;

				font-style: normal;



			}

			@font-face {
    

				font-family : "holtwood_one_scregular";

				src : url("../Resources/fonts/holtwoodonesc-webfont.woff2") format("woff2"),

				url("../Resources/fonts/holtwoodonesc-webfont.woff") format("woff");
				font-weight : normal;

				font-style : normal;


			
}

			html { 

				background : url(../Resources/images/Violet+Simple+Class+Schedule_upscaled_illustration_x4.png) no-repeat center center; 
				-webkit-background-size : contain;
				-moz-background-size : contain;
				-o-background-size : contain;
				background-size : 1920px;

			}

			.LogoUtama {

				margin-top : 40px;
				margin-left : 50px;
				width : 150px;
				-webkit-filter : drop-shadow(0px 3px 3px #000000);
				filter : drop-shadow(0px 3px 3px #000000); 

			}

			fieldset {

				width : 700px;
				height: 600px;
				background : #FFFFFF 0% 0% no-repeat padding-box;
				box-shadow : 10px 10px 6px #0000004F;
				border : 2px solid #FFA200;
				border-radius : 25px;
				opacity : 1;

			}

			.ContainerLogo {

				margin-top : 40px;
				margin-left : 200px;
				position : relative;
				width : 200px;
				height : 230px;

			}

			.ContainerLogoDepan {

				position: absolute;
				z-index: 2;
			}

			.ContainerLogoBelakang {

				position: absolute;
				z-index: 1;

			}

			.LogoKucing {

				width : 110px;
				-webkit-filter : drop-shadow(0px 3px 3px #000000);
				filter : drop-shadow(0px 3px 3px #000000);

			}

			.IconRegister {

				font-family : holtwood_one_scregular;
				letter-spacing : -3px;
				color : #4C9AE9;
				text-shadow : 0px 8px 4px #00000070;
				font-size : 58px;
				position : absolute;
				margin-top : 75px;
				left : -500px;
				width: 1000px;

			}

			::-webkit-input-placeholder {

				color: #00000029;
				font-family : acremedium;
				font-size : 15px;

			}

			.Email {

				width : 500px;
				height : 50px;
				box-shadow : 3px 3px 6px #00000029;
				border : 1px solid #00000029;
				border-radius : 15px;
				padding-left : 30px;
				font-family : acremedium;
				font-size : 15px;


			}

			.LabelFind {

				margin-right: 360px;
				width : 200px;

			}

			.Find {

				font-family : acremedium;
				font-size : 15px;
				color : #00000029;

			}

			.DivReset {

				float-right;
				margin-left : 365px;
				width : 220px;

			}

			.Reset {

				margin-top : 25px;
				width: 130px;
				height: 50px;
				background: #FF8000;
				box-shadow : 3px 3px 6px #00000073;
				border : 5px solid #FF8000;
				border-radius : 15px;
				position : relative;
				top : 3px;

			}

			.ResetLabel {

				text-align : center;
				font-family : acremedium;
				font-size : 15px;
				color : #000000;

			}


			.Radio1 {

				transform: scale(3);
				width : 50px;
				cursor: pointer;

			}


			.Radio2 {

				transform: scale(3);
				width : 50px;
				cursor: pointer;

			}

			.Reset1 {

				font-family : acremedium;
				font-size : 17px;
				color : #00000029;

			}

			.Reset2 {

				font-family : acremedium;
				font-size : 17px;
				color : #00000029;

			}

			.ContainerRadio {

				width : 500px;
				height : 110px;
				text-align : left;
				margin-right : 100px;

			}

		</style>

	</head>

	<body>

		<a href = "..\1. Home\Home.php"> <img src = "../Resources/images/LOGOANML.png" class = "LogoUtama"> </a>

		<center>

			<fieldset>

				<form>

					<div class = "ContainerLogo">

						<div class = "ContainerLogoDepan"> <img src = "../Resources/images/58a050065583a1291368eeb4.png" class = "LogoKucing"> </div>

						<div class = "ContainerLogoBelakang"> <label class = "IconRegister"> RESET PASSWORD </label> </div>


					</div>


					<div class = "LabelFind"> <label class = "Find"> <b> Find Your Account </b> </label> </div>

							<br>
							<br>
							<br>

					<div class = "ContainerRadio">

						<input type = "radio" name = "resets" class = "Radio1"> <label class = "Reset1"> <b> Reset password using email </b> </label>

								<br>
								<br>
								<br>
								<br>

						<input type = "radio" name = "resets" class = "Radio2"> <label class = "Reset2"> <b> Reset password using phone number </b> </label>

								<br>
					</div>

						<div class = "DivReset"> <button class = "Reset"> <label class = "ResetLabel"> Reset </label> </button> </div>

							<br>

				</form>

			</fieldset>

		</center>

			<br>
			<br>
			<br>
			<br>

	</body>

</html>