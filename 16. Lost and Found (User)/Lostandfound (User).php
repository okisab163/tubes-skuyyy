<?php

	session_start();

	include("ConfigLocal.php");

?>

<html>

	<head>
	
		<title> Lost And Found </title>

		<script src="http://code.jquery.com/jquery-latest.js"> </script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="crossorigin="anonymous"></script>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

		<script src="http://code.jquery.com/jquery-latest.js"> </script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="crossorigin="anonymous"></script>

		<script>

			$(document).ready(function() {

				$('.Select1Pet').hide();
				$('.Select2Pet').hide();
				$('.Select3Pet').hide();
				$('.Input1Pet').hide();

				$('[name="option_kategori"]').change(function() {

					if ($('[name="option_kategori"]').val() === "Breed" && $('[name="kategori_animals"]').val() === "Dog") {

						$('.Select1Pet').show(1000);
						$('.Input1Pet').hide(1000);
						$(".Input1Pet").val(""); 

					} else if ($('[name="option_kategori"]').val() === "Breed" && $('[name="kategori_animals"]').val() === "Cat") {

						$('.Select2Pet').show(1000);
						$('.Input1Pet').hide(1000);
						$(".Input1Pet").val(""); 

					} else if ($('[name="option_kategori"]').val() === "Breed" && $('[name="kategori_animals"]').val() === "Other Animals") {

						$('.Select3Pet').show(1000);
						$('.Input1Pet').hide(1000);
						$(".Input1Pet").val(""); 

					} else if ($('[name="option_kategori"]').val() === "Name") {

						$('.Select1Pet').hide(1000);
						$('.Select2Pet').hide(1000);
						$('.Select3Pet').hide(1000);
						$('.Input1Pet').show(1000);
						$(".Select1Pet").val(""); 
						$(".Select2Pet").val(""); 
						$(".Select3Pet").val(""); 

					}

				})
			});

		</script>

		<script>

			$(function() {

				$('.SelectPet option[value=Breed]').hide();

				$('.SelectAnimals').change(function() {

					if($('.SelectAnimals').val() == 'Other Animals' || $('.SelectAnimals').val() == 'Dog' || $('.SelectAnimals').val() == 'Cat') {

						$('.SelectPet option[value=Breed]').show();

					} 

				});

			});

		</script>

		<style type="text/css">

			fieldset {

				width : 1200px;
				height: 2200px;
				margin-top: 440px;
				background : #FFFFFF 0% 0% no-repeat padding-box;
				box-shadow : 10px 10px 6px #0000004F;
				border : 2px solid #FFA200;
				border-radius : 25px;
				opacity : 1;

			}

			.bgFooter {

				margin-top: 50px;
				background-image: url("../Resources/images/footer.jpg");
				background-repeat: no-repeat;
				background-size: 100% 100%;
				width : 100%;
				height: 550px;

			}

			.ContainerAnimals {

				width : 100%;
				margin-top: 150px;
				background : white;

			}

			.AnimalsLabel {

				font-family : acremedium;
				font-size : 45px;
				color: #4AA7E5;

			}

			.ImageAnimals {

				width: 1600px;
				height: 200px;
				box-shadow: 10px 10px 6px #000000CC;
				border-radius: 50px;
				opacity: 1;

			}

			.ButtonEdit:focus {

				outline: none;

			}

			.ButtonDelete:focus {

				outline: none;

			}

			.ButtonAdd:focus {

				outline: none;

			}

			.TulisanForm {

				font-family : acremedium;
				font-size : 20px;
				color: black;

			}

			.image-upload>input {

				display: none;

			}

			.image-upload {

				-webkit-filter : drop-shadow(0px 3px 3px #000000);
				filter : drop-shadow(0px 3px 3px #000000); 

			}

			.imageupload>input {

				display: none;

			}

			.imageupload {

				-webkit-filter : drop-shadow(0px 3px 3px #000000);
				filter : drop-shadow(0px 3px 3px #000000); 

			}

			.scroll::-webkit-scrollbar {

				width: 12px;
			}

			.scroll::-webkit-scrollbar-track {

				-webkit-box-shadow: inset 0 0 6px black; 
				border-radius: 10px;

			}

			.scroll::-webkit-scrollbar-thumb {

				border-radius: 10px;
				-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5); 

			}

			.dropbtn {

				border: none;
				font-family: acremedium;
				text-decoration: none;
				font-size: 33px;
				letter-spacing: 0;
				color: #E54A4A;
				opacity: 1;
				margin-right: 14px;
				background-color: Transparent;
    			background-repeat:no-repeat;
    			border: none;
    			cursor:pointer;

			}

			.dropdown {

				display: inline-block;
			}

			.dropdown-content {

				display: none;
				position: absolute;
				background-color: #f1f1f1;
				min-width: 160px;
				box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
				z-index: 1;
			}

			.dropdown-content a {

				color: black;
				font-size: 20px;
				letter-spacing: 0;
				padding: 12px 16px;
				text-decoration: none;
				display: block;
			}

			.dropdown-content a:hover {

				background-color: #ddd;

			}

			.dropdown:hover .dropdown-content {

				display: block;

			}

			* {

				margin : 0;
				padding : 0;
			}

			@font-face {

				font-family: "acremedium";
				src: url('../Resources/fonts/jonathan_ball_-_acre-medium-webfont.woff2') format('woff2'),
				url('../Resources/fonts/jonathan_ball_-_acre-medium-webfont.woff') format('woff');
				font-weight: normal;
				font-style: normal;

			}

			@font-face {

				font-family : "holtwood_one_scregular";
				src : url("../Resources/fonts/holtwoodonesc-webfont.woff2") format("woff2"),
				url("../Resources/fonts/holtwoodonesc-webfont.woff") format("woff");
				font-weight : normal;
				font-style : normal;

			}

			@font-face {

				font-family: 'autour_oneregular';
				src: url('../Resources/fonts/autourone-regular-webfont.woff2') format('woff2'),
				url('../Resources/fonts/autourone-regular-webfont.woff') format('woff');
				font-weight: normal;
				font-style: normal;

			}

			@font-face {

				font-family: 'primerprint';
				src: url('../Resources/fonts/primer_print-webfont.woff2') format('woff2'),
				url('../Resources/fonts/primer_print-webfont.woff') format('woff');
				font-weight: normal;
				font-style: normal;
			}

			.BackgroundAtas {

				width : 100%;
				height: 300%;
				background-image: url("../Resources/images/Violet+Simple+Class+Schedule_upscaled_illustration_x4.png");
				background-position: center;
				background-repeat: no-repeat;
				background-size: cover;

			}

			.ContainerNavbar {

				height: 10px;

			}

			.NavbarKiri {

				float: left;

			}

			.NavbarKanan {

				margin-top: 40px;
				width: 800px;
				float: right;

			}

			.TulisanNavbar {

				font-family: acremedium;
				text-decoration: none;
				font-size: 33px;
				letter-spacing: 0;
				color: #E54A4A;
				opacity: 1;

			}

			.TulisanNavbar:hover {

				font-family: acremedium;
				text-decoration: none;
				font-size: 33px;
				letter-spacing: 0;
				color: #F34D14;
				opacity: 1;

			}

			.TulisanNavbarTerpilih {

				font-family: acremedium;
				text-decoration: none;
				font-size: 33px;
				letter-spacing: 0;
				color: #4AA7E5;
				opacity: 1;

			}

			.LogoUtama {

				margin-top : 40px;
				margin-left : 50px;
				width : 150px;
				-webkit-filter : drop-shadow(0px 3px 3px #000000);
				filter : drop-shadow(0px 3px 3px #000000); 

			}

			.ContainerLogo {

				margin-top : 40px;
				margin-left : 200px;
				position : relative;
				width : 200px;
				height : 230px;
				position: absolute;
				top: 210px;
				right: 100px;

			}

			.ContainerLogoDepan {

				position: absolute;
				z-index: 2;

			}

			.ContainerLogoBelakang {

				position: absolute;
				z-index: 1;
				left: -890px;
				width: 900px;

			}

			.LogoKucing {

				width : 110px;
				-webkit-filter : drop-shadow(0px 3px 3px #000000);
				filter : drop-shadow(0px 3px 3px #000000);

			}

			.IconRegister {

				font-family : holtwood_one_scregular;
				letter-spacing : -3px;
				color : #4C9AE9;
				text-shadow : 0px 8px 4px #00000070;
				font-size : 60px;
				position : absolute;
				margin-top : 75px;
				right : -104px;

			}

			::-webkit-input-placeholder {

				color: #00000029;
				font-family : acremedium;
				font-size : 15px;

			}
	
			.LabelLearn {

				color: #FEBF56;
				font-family: primerprint;
				font-size: 15px;

			}

			.LabelMore {

				color: black;
				font-family: primerprint;
				font-size: 42px;
				text-decoration: none;

			}

			.LabelMore:hover {

				color: #FFAA00;
				font-family: primerprint;
				font-size: 42px;
				text-decoration: none;

			}

			.LabelLove {

				color: black;
				font-family: autour_oneregular;
				font-size: 44px;

			}

			.ContainerLost {

				margin-top: 10px;

			}

			.LostKiri {

				float: left;
				margin-top: 100px;

			}

			.LostKanan {

				float: right;
				width: 700px;
				margin-top: 50px;
				margin-right: 40px;

			}

			.GambarLost {

				width: 400px;
				margin-left: 30px;

			}

			.LostHeader {

				font-family : acremedium;
				font-size : 45px;
				color: #4AA7E5;

			}

			.LostLabel {

				margin-top: 10px;
				font-family : acremedium;
				font-size : 28px;
				color: #66747E;

			}

			.ContainerPet {

				width : 1200px;
				height: 1800px;
				margin-top: 100px;
				background: #FFFFFF;
				box-shadow: 10px 10px 6px #0000004F;
				border: 5px solid #FFA200;
				border-radius: 50px;
				opacity: 1;

			}

			.FindPet {

				font-family : autour_oneregular;
				font-size : 40px;
				color: #65B4E9;
				opacity: 1;

			}

			.SelectAnimals {

				width: 126.5px;
				height: 40px;
				margin-top: 10px;
				margin-left: 1px;
				border-top-left-radius: 10px;
				border-bottom-left-radius: 10px;
				border: 1.5px solid #FF7B00;
				opacity: 1;
				font-family : acremedium;
				font-size : 14px;
				color: black;
				padding-left: 10px;


			}

			.SelectPet {

				width: 100px;
				height: 40px;
				margin-top: 10px;
				border-left: none;
				border-right: 1.5px solid #FF7B00;
				border-top: 1.5px solid #FF7B00;
				border-bottom: 1.5px solid #FF7B00;
				opacity: 1;
				font-family : acremedium;
				font-size : 14px;
				color: black;
				padding-left: 10px;

			}


			.Input1Pet {

				width: 400px;
				height: 40px;
				margin-top: 10px;
				padding-left: 10px;
				border: 1.5px solid #FF7B00;
				border-left: none;
				border-right: none;
				opacity: 1;
				font-family : acremedium;
				font-size : 14px;
				color: black;

			}

			.Select1Pet {

				width: 400px;
				height: 40px;
				margin-top: 10px;
				padding-left: 10px;
				border: 1.5px solid #FF7B00;
				border-left: none;
				border-right: none;
				opacity: 1;
				font-family : acremedium;
				font-size : 14px;
				color: black;

			}

			.Select2Pet {

				width: 400px;
				height: 40px;
				margin-top: 10px;
				padding-left: 10px;
				border: 1.5px solid #FF7B00;
				border-left: none;
				border-right: none;
				opacity: 1;
				font-family : acremedium;
				font-size : 14px;
				color: black;

			}

			.Select3Pet {

				width: 400px;
				height: 40px;
				margin-top: 10px;
				padding-left: 10px;
				border: 1.5px solid #FF7B00;
				border-left: none;
				border-right: none;
				opacity: 1;
				font-family : acremedium;
				font-size : 14px;
				color: black;

			}

			.Input1Pet:focus {

				outline: none;

			}

			.SelectPet:focus {

				outline: none;

			}

			.SelectAnimals:focus {

				outline: none;

			}


			.Select1Pet:focus {

				outline: none;

			}

			.Select2Pet:focus {

				outline: none;

			}

			.Select3Pet:focus {

				outline: none;

			}

			.ButtonPet {

				width: 80px;
				height: 40px;
				margin-top: 10px;
				border-top-right-radius: 10px;
				border-bottom-right-radius: 10px;
				background-color: #FF7B00;
				border: 1.5px solid #FF7B00;
				opacity: 1;
				cursor: pointer;

			}

		</style>

	</head>

	<body>

		<div class = "BackgroundAtas">

			<div class = "ContainerNavbar">

				<div class = "NavbarKiri">

					<a href = "..\13. Home (User)\Home (User).php"> <img src = "../Resources/images/LOGOANML.png" class = "LogoUtama"> </a>
					
				</div>

				<div class = "NavbarKanan">

					<div style = "float: left;"> 

						<center>

							<a href = "..\14. Adopt A Pet (User)\Adoptapet (User).php" class = "TulisanNavbar"> Adopt <br> a pet </a> 

						</center>

					</div>

					<div style = "float: left; margin-left: 85px;">

						<center>

							<a href = "Lostandfound (User).php" class = "TulisanNavbarTerpilih"> Lost <br> and found </a>

						</center>

					</div>

					<div style = "float: left; margin-left: 85px; margin-top: 20px;">

						<center>

							<a href = "..\17. About us (User)\Aboutus (User).php" class = "TulisanNavbar"> About Us </a>

						</center>

					</div>

					<div style = "float: left; margin-left: 60px; margin-top: 20px;">

						<center>

							<div class="dropdown">

								<button class="dropbtn"> <?= $_SESSION["username"] ?> </button>

								<div class="dropdown-content">

									<a href = "..\19. Edit Account (User)\UpdateAccount (User).php"> Update Profil </a>
									<a href = "logout.php">Log Out</a>

								</div>

							</div>

						</center>

					</div>

				</div>

			</div>

			<center>

				<div class = "ContainerLogo">

					<div class = "ContainerLogoDepan"> <img src = "../Resources/images/58a050065583a1291368eeb4.png" class = "LogoKucing"> </div>

					<div class = "ContainerLogoBelakang"> <label class = "IconRegister"> LOST AND FOUND </label> </div>


				</div>

				<fieldset>

				<div style = "margin-top: 60px; margin-left: 60px; margin-right: 60px;">

					<div style = "float: left;">

						<label class = "FindPet"> <b> Find My Pet </b> </label>

					</div>

					<div style = "float: right;">

						<div style = "float: left;">

							<select class = "SelectAnimals" name = "kategori_animals" id = "Binatang">

								<option disabled selected> Pet Category </option>
								<option value = "Dog"> Dog </option>
								<option value = "Cat"> Cat </option>
								<option value = "Other Animals"> Other Animals </option>

							</select>

						</div>

						<div style = "float: left;">

							<select class = "SelectPet" name = "option_kategori">

								<option disabled selected> Search By </option>
								<option value = "Name"> Name </option>
								<option value = "Breed"> Breed </option>

							</select>

						</div>

						<div style = "float: left;" class = "input-div"> 

							<input type = "text" placeholder = "Insert The Keywords" class = "Input1Pet">

						</div>

						<div style = "float: left;" class = "select-div">

							<input list = "BreedDogs" class = "Select1Pet">

								<datalist id = "BreedDogs">

									<script>

									var dogs = 	["AFFENPINSCHER", "AFGHAN HOUND", "AIREDALE TERRIER", "AKITA", "ALASKAN MALAMUTE", "ALPINE DACHSBRACKE", "AMERICAN AKITA", "AMERICAN COCKER SPANIEL", "AMERICAN FOXHOUND", "AMERICAN STAFFORDSHIRE TERRIER", "AMERICAN WATER SPANIEL", "APPENZELL CATTLE DOG", "ARIEGE POINTING DOG", "ARIEGEOIS", "ARTOIS HOUND", "ATLAS MOUNTAIN DOG (AIDI)", "AUSTRALIAN CATTLE DOG", "AUSTRALIAN KELPIE", "AUSTRALIAN SHEPHERD", "AUSTRALIAN SILKY TERRIER", "AUSTRALIAN STUMPY TAIL CATTLE DOG", "AUSTRALIAN TERRIER", "AUSTRIAN  PINSCHER", "AUSTRIAN BLACK AND TAN HOUND", "AUVERGNE POINTER", "AZAWAKH", "BASENJI", "BASSET FAUVE DE BRETAGNE", "BASSET HOUND", "BAVARIAN MOUNTAIN SCENT HOUND", "BEAGLE", "BEAGLE HARRIER", "BEARDED COLLIE", "BEDLINGTON TERRIER", "BELGIAN SHEPHERD DOG", "BERGAMASCO SHEPHERD DOG", "BERGER DE BEAUCE", "BERNESE MOUNTAIN DOG", "BICHON FRISE", "BILLY", "BLACK AND TAN COONHOUND", "BLOODHOUND", "BLUE GASCONY BASSET", "BLUE GASCONY GRIFFON", "BLUE PICARDY SPANIEL", "BOHEMIAN WIRE-HAIRED POINTING GRIFFON												", "BOLOGNESE", "BORDER COLLIE", "BORDER TERRIER", "BORZOI - RUSSIAN HUNTING SIGHTHOUND", "BOSNIAN AND HERZEGOVINIAN - CROATIAN SHEPHERD DOG", "BOSNIAN BROKEN-HAIRED HOUND - CALLED BARAK", "BOSTON TERRIER", "BOURBONNAIS POINTING DOG", "BOUVIER DES ARDENNES", "BOUVIER DES FLANDRES", "BOXER", "BRAZILIAN TERRIER", "BRIARD", "BRIQUET GRIFFON VENDEEN", "BRITTANY SPANIEL", "BROHOLMER", "BULL TERRIER", "BULLDOG", "BULLMASTIFF", "BURGOS POINTING DOG", "CAIRN TERRIER", "CANAAN DOG", "CANADIAN ESKIMO DOG", "CANARIAN WARREN HOUND", "CASTRO LABOREIRO DOG", "CATALAN SHEEPDOG", "CAUCASIAN SHEPHERD DOG", "CAVALIER KING CHARLES SPANIEL", "CENTRAL ASIA SHEPHERD DOG", "CESKY TERRIER", "CHESAPEAKE BAY RETRIEVER", "CHIHUAHUA", "CHINESE CRESTED DOG", "CHOW CHOW", "CIMARRÃ“N URUGUAYO", "CIRNECO DELL'ETNA", "CLUMBER SPANIEL", "COARSE-HAIRED STYRIAN HOUND", "COLLIE ROUGH", "COLLIE SMOOTH", "CONTINENTAL TOY SPANIEL", "COTON DE TULEAR", "CROATIAN SHEPHERD DOG", "CURLY COATED RETRIEVER", "CZECHOSLOVAKIAN WOLFDOG", "DACHSHUND												", "DALMATIAN", "DANDIE DINMONT TERRIER", "DANISH-SWEDISH FARMDOG", "DEERHOUND", "DEUTSCH LANGHAAR", "DEUTSCH STICHELHAAR", "DOBERMANN", "DOGO ARGENTINO", "DOGO CANARIO", "DOGUE DE BORDEAUX", "DRENTSCHE PARTRIDGE DOG", "DREVER", "DUTCH SCHAPENDOES", "DUTCH SHEPHERD DOG", "DUTCH SMOUSHOND", "EAST SIBERIAN LAIKA", "ENGLISH COCKER SPANIEL", "ENGLISH FOXHOUND", "ENGLISH POINTER", "ENGLISH SETTER", "ENGLISH SPRINGER SPANIEL", "ENGLISH TOY TERRIER (BLACK &TAN)", "ENTLEBUCH CATTLE DOG", "ESTRELA MOUNTAIN DOG", "EURASIAN", "FAWN BRITTANY GRIFFON", "FIELD SPANIEL", "FILA BRASILEIRO", "FINNISH HOUND", "FINNISH LAPPONIAN DOG", "FINNISH SPITZ", "FLAT COATED RETRIEVER", "FOX TERRIER (SMOOTH)", "FOX TERRIER (WIRE)", "FRENCH BULLDOG", "FRENCH POINTING DOG - GASCOGNE TYPE", "FRENCH POINTING DOG - PYRENEAN TYPE", "FRENCH SPANIEL", "FRENCH TRICOLOUR HOUND", "FRENCH WATER DOG", "FRENCH WHITE & BLACK HOUND", "FRENCH WHITE AND ORANGE HOUND", "FRISIAN WATER DOG", "GASCON SAINTONGEOIS", "GERMAN HOUND", "GERMAN HUNTING TE												RRIER", "GERMAN PINSCHER", "GERMAN SHEPHERD DOG", "GERMAN SHORT- HAIRED POINTING DOG", "GERMAN SPANIEL", "GERMAN SPITZ", "GERMAN WIRE- HAIRED POINTING DOG", "GIANT SCHNAUZER", "GOLDEN RETRIEVER", "GORDON SETTER", "GRAND BASSET GRIFFON VENDEEN", "GRAND GRIFFON VENDEEN", "GREAT ANGLO-FRENCH TRICOLOUR HOUND", "GREAT ANGLO-FRENCH WHITE & ORANGE HOUND", "GREAT ANGLO-FRENCH WHITE AND BLACK HOUND", "GREAT DANE", "GREAT GASCONY BLUE", "GREAT SWISS MOUNTAIN DOG", "GREENLAND DOG", "GREYHOUND", "GRIFFON BELGE", "GRIFFON BRUXELLOIS", "GRIFFON NIVERNAIS", "HALDEN HOUND", "HAMILTONSTÃ–VARE", "HANOVERIAN SCENT HOUND", "HARRIER", "HAVANESE", "HELLENIC HOUND", "HOKKAIDO", "HOVAWART", "HUNGARIAN GREYHOUND", "HUNGARIAN HOUND - TRANSYLVANIAN SCENT HOUND", "HUNGARIAN SHORT-HAIRED POINTER (VIZSLA)", "HUNGARIAN WIRE-HAIRED POINTER", "HYGEN HOUND", "IBIZAN PODENCO", "ICELANDIC SHEEPDOG", "IRISH GLEN OF IMAAL TERRIER", "IRISH RED AND WHITE SETTER", "IRISH RED SETTER", "IRISH SOFT COATED WHEATEN TERRIER", "IRISH TERRIER", "												IRISH WATER SPANIEL", "IRISH WOLFHOUND", "ISTRIAN SHORT-HAIRED HOUND", "ISTRIAN WIRE-HAIRED HOUND", "ITALIAN CANE CORSO", "ITALIAN POINTING DOG", "ITALIAN ROUGH-HAIRED SEGUGIO", "ITALIAN SHORT-HAIRED SEGUGIO", "ITALIAN SIGHTHOUND", "ITALIAN SPINONE", "ITALIAN VOLPINO", "JACK RUSSELL TERRIER", "JAPANESE CHIN", "JAPANESE SPITZ", "JAPANESE TERRIER", "JÃ„MTHUND", "KAI", "KANGAL SHEPHERD DOG", "KARELIAN BEAR DOG", "KARST SHEPHERD DOG", "KERRY BLUE TERRIER", "KING CHARLES SPANIEL", "KISHU", "KLEINER MÃœNSTERLÃ„NDER", "KOMONDOR", "KOREA JINDO DOG", "KROMFOHRLÃ„NDER", "KUVASZ", "LABRADOR RETRIEVER", "LAKELAND TERRIER", "LANCASHIRE HEELER", "LANDSEER (EUROPEAN CONTINENTAL TYPE)", "LAPPONIAN HERDER", "LARGE MUNSTERLANDER", "LEONBERGER", "LHASA APSO", "LITTLE LION DOG", "LONG-HAIRED PYRENEAN SHEEPDOG", "MAJORCA MASTIFF", "MAJORCA SHEPHERD DOG", "MALTESE", "MANCHESTER TERRIER", "MAREMMA AND THE ABRUZZES SHEEPDOG", "MASTIFF", "MEDIUM-SIZED ANGLO-FRENCH HOUND", "MINIATURE BULL TERRIER", "MINIATURE PINSCHER", "MI												NIATURE SCHNAUZER", "MONTENEGRIN MOUNTAIN HOUND", "MUDI", "NEAPOLITAN MASTIFF", "NEDERLANDSE KOOIKERHONDJE", "NEWFOUNDLAND", "NORFOLK TERRIER", "NORMAN ARTESIEN BASSET", "NORRBOTTENSPITZ", "NORWEGIAN BUHUND", "NORWEGIAN ELKHOUND BLACK", "NORWEGIAN ELKHOUND GREY", "NORWEGIAN HOUND", "NORWEGIAN LUNDEHUND", "NORWICH TERRIER", "NOVA SCOTIA DUCK TOLLING RETRIEVER", "OLD DANISH POINTING DOG", "OLD ENGLISH SHEEPDOG", "OTTERHOUND", "PARSON RUSSELL TERRIER", "PEKINGESE", "PERUVIAN HAIRLESS DOG", "PETIT BASSET GRIFFON VENDEEN", "PETIT BRABANÃ‡ON", "PHARAOH HOUND", "PICARDY SHEEPDOG", "PICARDY SPANIEL", "POITEVIN", "POLISH GREYHOUND", "POLISH HOUND", "POLISH HUNTING DOG", "POLISH LOWLAND SHEEPDOG", "PONT-AUDEMER SPANIEL", "POODLE", "PORCELAINE", "PORTUGUESE POINTING DOG", "PORTUGUESE SHEEPDOG", "PORTUGUESE WARREN HOUND-PORTUGUESE PODENGO", "PORTUGUESE WATER DOG", "POSAVATZ HOUND", "PUDELPOINTER", "PUG", "PULI", "PUMI", "PYRENEAN MASTIFF", "PYRENEAN MOUNTAIN DOG", "PYRENEAN SHEEPDOG - SMOOTH FACED", "RAFEIRO O												F ALENTEJO", "RHODESIAN RIDGEBACK", "ROMAGNA WATER DOG", "ROMANIAN BUCOVINA SHEPHERD", "ROMANIAN CARPATHIAN SHEPHERD DOG", "ROMANIAN MIORITIC SHEPHERD DOG", "ROTTWEILER", "RUSSIAN BLACK TERRIER", "RUSSIAN TOY", "RUSSIAN-EUROPEAN LAIKA", "SAARLOOS WOLFHOND", "SAINT GERMAIN POINTER", "SAINT MIGUEL CATTLE DOG", "SALUKI", "SAMOYED", "SCHILLERSTÃ–VARE", "SCHIPPERKE", "SCHNAUZER", "SCOTTISH TERRIER", "SEALYHAM TERRIER", "SEGUGIO MAREMMANO", "SERBIAN HOUND", "SERBIAN TRICOLOUR HOUND", "SHAR PEI", "SHETLAND SHEEPDOG", "SHIBA", "SHIH TZU", "SHIKOKU", "SIBERIAN HUSKY", "SKYE TERRIER", "SLOUGHI", "SLOVAKIAN CHUVACH", "SLOVAKIAN HOUND", "SMALL BLUE GASCONY", "SMALL SWISS HOUND", "SMÃ…LANDSSTÃ–VARE", "SOUTH RUSSIAN SHEPHERD DOG", "SPANISH GREYHOUND", "SPANISH HOUND", "SPANISH MASTIFF", "SPANISH WATER DOG", "ST. BERNARD", "STABIJHOUN", "STAFFORDSHIRE BULL TERRIER", "SUSSEX SPANIEL", "SWEDISH LAPPHUND", "SWEDISH VALLHUND", "SWISS HOUND", "TAIWAN DOG", "TATRA SHEPHERD DOG", "THAI BANGKAEW DOG", "THAI RIDGEBACK DOG												", "TIBETAN MASTIFF", "TIBETAN SPANIEL", "TIBETAN TERRIER", "TOSA", "TYROLEAN HOUND", "WEIMARANER", "WELSH CORGI (CARDIGAN)", "WELSH CORGI (PEMBROKE)", "WELSH SPRINGER SPANIEL", "WELSH TERRIER", "WEST HIGHLAND WHITE TERRIER", "WEST SIBERIAN LAIKA", "WESTPHALIAN DACHSBRACKE", "WHIPPET", "WHITE SWISS SHEPHERD DOG", "WIRE-HAIRED POINTING GRIFFON KORTHALS", "WIREHAIRED SLOVAKIAN POINTER", "XOLOITZCUINTLE", "YORKSHIRE TERRIER", "YUGOSLAVIAN SHEPHERD DOG - SHARPLANINA" ];

											$.each(dogs, function (index, value) {

												$('#BreedDogs').append($('<option/>', {

													value: value,
													text : value 

												}));
											});

									</script>

								</datalist>

							<input list = "BreedCats" class = "Select2Pet">

								<datalist id = "BreedCats">

											<script>

											var cats = 	["Abyssinian", "Aegean", "American Bobtail", "American Curl", "American Shorthair", "American Wirehair", "Aphrodite Giant", "Arabian Mau", "Asian", "Asian Semi-longhair", "Australian Mist", "Balinese", "Bambino", "Bengal", "Birman", "Bombay", "Brazilian Shorthair", "British Longhair", "British Semi-longhair", "British Shorthair", "Burmese", "Burmilla", "California Spangled", "Chantilly-Tiffany", "Chartreux", "Chausie", "Colorpoint Shorthair", "Cornish Rex", "Cymric", "Cyprus", "Devon Rex", "Donskoy", "Dragon Li", "Dwelf", "Egyptian Mau", "European Shorthair", "Exotic Shorthair", "Foldex", "German Rex", "Havana Brown", "Highlander", "Himalayan", "Japanese Bobtail", "Javanese", "Karelian Bobtail", "Khao Manee", "Korat", "Korean Bobtail", "Korn Ja", "Kurilian Bobtail", "LaPerm", "Lykoi", "Maine Coon", "Manx", "Mekong Bobtail", "Minskin", "Munchkin", "Napoleon", "Nebelung", "Norwegian Forest Cat", "Ocicat", "Ojos Azules", "Oregon Rex", "Oriental Bicolor", "Oriental Longhair", "Oriental Shor													thair", "Persian (modern)", "Persian (traditional)", "Peterbald", "Pixie-bob", "Ragamuffin", "Ragdoll", "Russian Blue", "Russian White", "Black", "and Tabby", "Sam Sawet", "Savannah", "Scottish Fold", "Selkirk Rex", "Serengeti", "Serrade Petit", "Siamese", "Siberian", "Singapura", "Snowshoe", "Sokoke", "Somali", "Sphynx", "Suphalak", "Thai", "Thai Lilac", "Tonkinese", "Toyger", "Turkish Angora", "Turkish Van", "Ukrainian Levkoy", "Wila Krungthep", "York Chocolate" ];

											$.each(cats, function (index, value) {

												$('#BreedCats').append($('<option/>', {

													value: value,
													text : value 

												}));
											});

											</script>

								</datalist>

							<input list = "BreedOthers" class = "Select3Pet">

								<datalist id = "BreedOthers">

											<script>

											var other = 	["African buffalo (Syncerus caffer)", "African bush elephant (Loxodonta africana)", "African forest elephant (L. cyclotis)", "African clawed frog (Xenopus laevis)", "African giant snail (Achatina achatina)", "Alligator gar (Atractosteus spatula)", "Cuban gar (A. tristoechus) and Tropical gar (A. tropicus)", "Alpaca (Vicugna pacos)", "American (Alligator mississippiensis) and Chinese alligators (A. sinensis)", "American bison (Bison bison)", "American bullfrog (Lithobates catesbeianus)", "American cockroach (Periplaneta americana)", "Arabian oryx (Oryx leucoryx)", "Arabian ostrich (Struthio camelus syriacus)†", "Arapaima (Arapaima gigas)", "Arctic (Vulpes lagopus)", "Red (V. vulpes) Fennec (V. zerda)", "Corsac (V. corsac)", "Pale (V. pallida)", "Swift (V. velox) and Kit foxes (V. macrotis)", "Argentine and Cranwell's horned frogs (Ceratophrys ornata and C. cranwelli)", "Arrau turtle (Podocnemis expansa)", "Asiatic honey bee (Apis cerana)", "including subspecies Indian (A. cerana indica) 													and Japanese honey bees (A. cerana japonica)", "Atlantic horseshoe crab (Limulus polyphemus)", "Atlantic Salmon (Salmo salar)", "Brown trout (S. trutta)", "Australian green tree frog (Litoria caerulea)", "Axolotl (Ambystoma mexicanum)", "Bald eagle (Haliaeetus leucocephalus)", "Bali cattle (Bos javanicus domesticus)", "Ball python (Python regius)", "Barbary dove (Streptopelia roseogrisea risoria)", "Barbary sheep (Ammotragus lervia)", "Barramundi (Lates calcarifer)", "Bat-eared fox (Otocyon megalotis)", "Beech marten (Martes foina)", "Sable (M. zibellina) and Yellow-throated marten (M. flavigula)", "Bigfin reef squid (Sepioteuthis lessoniana)", "Bighead carp (Hypophthalmichthys nobilis)", "Black and Grey francolin (Francolinus francolinus and F. pondicerianus)", "Black carp (Mylopharyngodon piceus)", "Black rhinoceros (Diceros bicornis)", "Black soldier fly (Hermetia illucens)", "Blackbuck (Antilope cervicapra)", "Black-legged seriema (Chunga burmeisteri)", "Blue (Connochaetes taurinus) and Black 													wildebeests (C. gnou)", "Blue swimmer crab (Portunus armatus)", "Bontebok (Damaliscus pygargus) including subspecies Blesbok (D. p. phillipsi)", "Bubal hartebeest (Alcelaphus buselaphus buselaphus)†", "Budgerigar (Melopsittacus undulatus)", "Buff-tailed bumblebee (Bombus terrestris)", "Caracal (Caracal caracal)", "Carpet python (Morelia spilota)", "Cat (Felis catus)", "Cattle (Bos taurus)", "Central bearded dragon (Pogona vitticeps)", "Chicken (Gallus gallus domesticus)", "Chinese (Bambusicola thoracicus) and Mountain bamboo partridges (B. fytchii)", "Chinese (Rhizomys sinensis) and Large bamboo rats (R. sumatrensis)", "Chinese cobra (Naja atra)", "Chinese edible frog (Hoplobatrachus rugulosus)", "Indus Valley bullfrog (H. tigerinus)", "Chinese hamster (Cricetulus griseus)", "Chinese pond turtle (Mauremys reevesii)", "Yellow pond turtle (M. mutica)", "Chinese softshell turtle (Pelodiscus sinensis)", "Chinese spiny frog (Quasipaa spinosa)", "Chinkara (Gazella bennettii)", "Chital (Axis axis)", "Co														bia (Rachycentron canadum)", "Cochineal (Dactylopius coccus)", "Cockatiel (Nymphicus hollandicus)", "Collared peccary (Pecari tajacu)", "Common (Lampropeltis getula)", "Grey-banded kingsnakes (L. alterna) and Milk snake (L. triangulum)", "Common (Octopus vulgaris)", "Common Sydney (O. tetricus)", "Big blue (O. cyanea)", "Mexican four-eyed (O. maya)", "California two-spot (O. bimaculoides)", "Gould (O. mimus)", "Long arm (O. minor) and Caribbean reef octopus (O. briareus)", "Common (Phasianus colchicus) and Green pheasants (P. versicolor)", "Common carp (Cyprinus carpio)", "Common degu (Octodon degus)", "Common eland (Taurotragus oryx)", "Common fruit fly (Drosophila melanogaster)", "Common hill myna (Gracula religiosa)", "Common kusimanse (Crossarchus obscurus)", "Common periwinkle (Littorina littorea)", "Common quail (Coturnix coturnix) and Japanese quail (C. japonica)", "Common snapping turtle (Chelydra serpentina)", "Companion parrot (Order Psittaciformes)", "Corn snake (Pantherophis gutttatus														)", "Coypu (Myocastor coypus)", "Crab-eating fox (Cerdocyon thous)", "Crab-eating frog (Fejervarya cancrivora)", "Crested gecko (Rhacodactylus ciliatus)", "Crimson finch (Neochmia phaeton)", "Crimson", "Eastern", "Western", "Green", "Pale-headed and Northern rosellas (Platycercus elegans", "P. eximius", "P. icterotis", "P. caledonicus", "P. adscitus and P. venustus)", "Crucian carp (Carassius carassius)", "Desmarest's hutia (Capromys pilorides)", "Diamond dove (Geopelia cuneata)", "Dingo (Canis lupus dingo)", "New Guinea singing dog (C. l. hallstromi)", "Eurasian wolf (C. l. lupus)", "Steppe wolf (C. l. campestris)", "Mongolian wolf (C. l. chanco)", "Arabian wolf (C. l. arabs)", "Arctic wolf (C. l. arctos)", "Northwestern wolf (C. l. occidentalis)", "Dog (Canis lupus familiaris)", "Domestic Bactrian camel (Camelus bactrianus)", "Domestic canary (Serinus canaria domestica)", "Domestic duck (Anas platyrhynchos domesticus)", "Domestic goose (Anser anser domesticus and Anser cygnoides domesticus)", "														Domestic guinea pig (Cavia porcellus)", "Domestic guineafowl (Numida meleagris)", "Domestic hedgehog (Atelerix albiventris domestica)", "Domestic mink (Neovison vison domesticus)", "Domestic muscovy duck (Cairina moschata domestica)", "Domestic pig (Sus scrofa domesticus)", "Domestic pigeon (Columba livia domestica)", "Domestic rabbit (Oryctolagus cuniculus)", "Domestic silkmoth (Bombyx mandarina mori)", "Domestic skunk (Mephitis mephitis)", "Domestic turkey (Meleagris gallopavo)", "Domesticated red fox (Vulpes vulpes)", "Donkey (Equus africanus asinus)", "Dorcas gazelle (Gazella dorcas)", "Dromedary camel (Camelus dromedarius)", "Dubia roach (Blaptica dubia)", "Dwarf (Moschus berezovskii)", "Alpine (M. chrysogaster)", "White-bellied (M. leucogaster) and Siberian musk deer (M. moschiferus)", "Dwarf hamsters (Phodopus spp.)", "Dyeing (Dendrobates tinctorius)", "Green and black (D. auratus)", "Yellow-banded poison dart frog (D. leucomelas)", "Eastern bearded dragon (Pogona barbata)", "Eclectus parr														ot (Eclectus roratus)", "Edible-nest swiftlet (Aerodramus fuciphagus)", "Egyptian goose (Alopochen aegyptiacus)", "Egyptian mongoose (Herpestes ichneumon)", "Elk (Cervus canadensis) including subspecies Rocky Mountain elk (C. c. nelsoni)", "Roosevelt elk (C. c. roosevelti)", "Tule elk (C. c. nannodes)", "Manitoban elk (C. c. manitobensis)", "Emu (Dromaius novaehollandiae)", "Eurasian eagle-owl (Bubo bubo)", "Eurasian harvest mouse (Micromys minutus)", "Eurasian lynx (Lynx lynx) including subspecies Siberian lynx (L. lynx wrangeli) and Carpathian lynx (L. lynx carpathicus)", "European (Homarus gammarus) and American lobsters (H. americanus)", "European bass (Dicentrarchus labrax)", "European eel (Anguilla anguilla)", "American eel (A. rostrata)", "Short-finned eel (A. australis)", "New Zealand longfin eel (Anguilla dieffenbachii) and Japanese eel (A. japonica)", "European goldfinch (Carduelis carduelis)", "European medicinal leech (Hirudo medicinalis)", "European mink (Mustela lutreola)", "Europea														n mouflon (Ovis orientalis musimon)", "Fallow deer (Dama dama)", "Fancy mouse and laboratory mouse (Mus musculus domestica)", "Fancy rat and laboratory rat (Rattus norvegicus domestica)", "Ferret (Mustela putorius furo)", "Fisher (Pekania pennanti)", "Fishing (Prionailurus viverrinus) and Rusty-spotted cats (P. rubiginosus)", "Flame jellyfish (Rhopilema esculentum)", "Flathead grey mullet (Mugil cephalus)", "Florida manatee (Trichechus manatus latirostris)", "Fringe-eared oryx (Oryx beisa callotis) and Common beisa oryx (O. b. beisa)", "Fuegian dog (Lycalopex culpaeus)†", "Gambian pouched rat (Cricetomys gambianus) and Emin's pouched rat (C. emini)", "Garden snail (Cornu aspersum)", "Gayal (Bos frontalis)", "Gemsbok (Oryx gazella)", "Genets: Common (Genetta genetta)", "Cape (G. tigrina)", "Pardine (G. pardina) and Rusty-spotted (G. maculata)", "Giant barb (Catlocarpio siamensis)", "Giant East African snail (Lissachatina fulica)", "Giant eland (Taurotragus derbianus)", "Giant gourami (Osphronemus 														goramy)", "Giant Pacific octopus (Enteroctopus dofleini)", "Glossy ibis (Plegadis falcinellus)", "Goat (Capra aegagrus hircus)", "Golden coin turtle (Cuora trifasciata)", "Golden eagle (Aquila chrysaetos)", "Golden hamster (Mesocricetus auratus)", "Golden pheasant (Chrysolophus pictus)", "Goldfish (Carassius auratus)", "Gouldian finch (Erythrura gouldiae)", "Grass carp (Ctenopharyngodon idella)", "Greater (Galictis vittata) and Lesser grisons (G. cuja)", "Greater cane rat (Thryonomys swinderianus)", "Greater kudu (Tragelaphus strepsiceros)", "Nyala (T. angasii)", "Cape bushbuck (T. sylvaticus)", "Greater rhea (Rhea americana)", "Green chromide (Etroplus suratensis)", "Green iguana (Iguana iguana)", "Green tree python (Morelia viridis)", "Grey partridge (Perdix perdix)", "Guanaco (Lama guanicoe)", "Guppy (Poecilia reticulata)", "Harlequin (Coturnix delegorguei)", "Rain (C. coromandelica)", "and Stubble quails (C. pectoralis)", "Harris's hawk (Parabuteo unicinctus)", "Himalayan monal (Lophophorus i														mpejanus)", "Horned screamer (Anhima cornuta)", "Horse (Equus ferus caballus)", "House cricket (Acheta domesticus)", "Indian elephant (Elephas maximus indicus)", "Indian gray mongoose (Herpestes edwardsii)", "Indian palm squirrel (Funambulus palmarum)", "Indian peafowl (Pavo cristatus)", "Indian rose-ringed parakeet (Psittacula krameri manillensis) and African rose-ringed parakeet (P. k. krameri)", "Iridescent shark (Pangasianodon hypophthalmus) and Mekong giant catfish (P. gigas)", "Island (Urocyon littoralis) and Gray foxes (U. cinereoargenteus)", "Japanese (Seriola quinqueradiata)", "Greater (S. dumerili)", "and Yellowtail amberjacks (S. lalandi); Almaco jack (S. rivoliana)", "Japanese horseshoe crab (Tachypleus tridentatus) and Indo-Pacific horseshoe crab (Tachypleus gigas)", "Japanese", "Neotropic and great cormorants (Phalacrocorax capillatus", "P. brasilianus and P. carbo)", "Java sparrow (Lonchura oryzivora)", "Kalij (Lophura leucomelanos)", "Swinhoe's (L. swinhoii)", "and Edwards's pheas														ants (L. edwardsi)", "Keeled box turtle (Cuora mouhotii)", "King quail (Excalfactoria chinensis)", "Kissing gourami (Helostoma temminckii)", "Koi (Cyprinus rubrofuscus)", "Leopard cat (Prionailurus bengalensis)", "Leopard gecko (Eublepharis macularius)", "Lesser bamboo rat (Cannomys badius)", "Llama (Lama glama)", "Long-tailed chinchilla (Chinchilla lanigera)", "Lowland (Cuniculus paca) and Mountain pacas (C. taczanowskii)", "Madagascar hissing cockroach (Gromphadorhina portentosa)", "Major Mitchell's cockatoo (Lophochroa leadbeateri)", "Malabar (Epinephelus malabaricus)", "Giant (E. lanceolatus)", "Greasy (Epinephelus tauvina)", "Areolate (Epinephelus areolatus)", "Dusky (Epinephelus marginatus) and Orange-spotted groupers (E. coioides)", "Malayan porcupine (Hystrix brachyura)", "Mandarin duck (Aix galericulata)", "Mangrove horseshoe crab (Carcinoscorpius rotundicauda)", "Mealworm and Superworm (Tenebrio molitor and Zophobas morio)", "Meerkat (Suricata suricatta)", "Milkfish (Chanos chanos)", "M														ongolian gerbil (Meriones unguiculatus) et al.", "Montezuma quail (Cyrtonyx montezumae)", "Moose (Alces alces) including subspecies Alaska moose (A. a. gigas)", "Mopane moth (Gonimbrasia belina)", "Mountain quail (Oreortyx pictus)", "Mud crab (Scylla serrata)", "Muskox (Ovibos moschatus)", "Mute swan (Cygnus olor)", "Nile (Crocodylus niloticus)", "West African (C. suchus)", "Siamese", "(C. siamensis)", "Cuban (C. rhombifer)", "Morelet's (C. moreletii)", "Orinoco (C. intermedius)", "American (C. acutus)", "Freshwater (C. johnsoni)", "Philippine (C. mindorensis)", "Saltwater (C. porosus)", "New Guinea (C. novaeguineae) and Mugger crocodile (C. palustris)", "Nilgai (Boselaphus tragocamelus)", "North African (Struthio camelus camelus) and Masai ostriches (S. c. massaicus)", "Northeast African cheetah (Acinonyx jubatus soemmeringii) and Asiatic cheetah (A. jubatus venaticus)", "Northeastern (Ctenosaura acanthura)", "Black (C. similis)", "and Mexican spiny-tailed iguanas (C. pectinata)", "Northern bobw														hite (Colinus. virginianus) including subspecies Masked bobwhite (C. v. ridgwayi)", "Northern goshawk (Accipiter gentilis)", "Northern pike (Esox lucius)", "Northern red snapper (Lutjanus campechanus)", "Norway lemming (Lemmus lemmus)", "Nubian giraffe (Giraffa camelopardalis camelopardalis)", "Nubian ibex (Capra nubiana)", "Oriental darter (Anhinga melanogaster)", "Pacific beetle cockroach (Diploptera punctata)", "Pacific hagfish (Eptatretus stoutii) and Inshore hagfish (E. burgeri)", "Pale and lesser gerbils (Gerbillus perpallidus", "G. gerbillus)", "Palm cockatoo (Probosciger aterrimus)", "Patagonian weasel (Lyncodon patagonicus)", "Pekin robin (Leiothrix lutea)", "Pig frog (Lithobates grylio)", "Pin-tailed parrotfinch (Erythrura prasina)", "Piranha (Pygocentrus nattereri and P. piraya)", "Pool (Pelophylax lessonae) and Marsh frogs (P. ridibundus)", "Puna ibis (Plegadis ridgwayi)", "Raccoon (Procyon lotor)", "Raccoon dog (Nyctereutes procyonoides) including subspecies Japanese raccoon dog (N. 														p. viverrinus)", "Rainbow trout (Oncorhynchus mykiss)", "Masu salmon (O. masou)", "Chinook salmon (O. tshawytscha)", "Rainbowfish (Melanotaeniidae)", "Rankin's dragon (Pogona henrylawsoni)", "Red (Paralithodes camtschaticus) and Blue king crabs (P. platypus)", "Red deer (Cervus elaphus)", "Red flour beetle (Tribolium castaneum)", "Red-eared", "yellow-bellied", "and Cumberland sliders (Trachemys scripta elegans", "T. s. scripta", "and T. s. troostii)", "Red-legged (Alectoris rufa)", "Chukar (A. chukar)", "Philby's (A. philbyi)", "Arabian (A. melanocephala) and Barbary partridges (A. barbara)", "Red-legged seriema (Cariama cristata)", "Red-necked wallaby (Macropus rufogriseus)", "Red-tailed black cockatoo (Calyptorhynchus banksii)", "Red-winged tinamou (Rhynchotus rufescens)", "Ring-tailed cat (Bassariscus astutus)", "Cacomistle (B. sumichrasti)", "Roman snail (Helix pomatia)", "Rosy-faced", "yellow-collared", "and Fischer's lovebirds (Agapornis roseicollis", "A. personatus and A. fischeri)", "Rudd														y mongoose (Herpestes smithii)", "Javan mongoose (H. javanicus)", "Sable (Hippotragus niger) and Roan antelopes (H. equinus)", "Salmon-crested cockatoo (Cacatua moluccensis)", "Scaled (Callipepla squamata)", "Gambel's (C. gambelii)", "and California quails (C. californica)", "Scaly-breasted munia (Lonchura punctulata)", "Scimitar oryx (Oryx dammah)", "Sea-monkey (Artemia nyos)", "Semi-domesticated reindeer (Rangifer tarandus)", "Serval (Leptailurus serval)", "Sheep (Ovis aries)", "Short-tailed chinchilla (Chinchilla chinchilla)", "Siamese fighting fish (Betta splendens)", "Siberian chipmunk (Eutamias sibiricus)", "Siberian weasel (Mustela sibirica)", "Sika deer (Cervus nippon)", "Silver carp (Hypophthalmichthys molitrix)", "Small-billed tinamou (Crypturellus parvirostris)", "Smooth-coated otter (Lutrogale perspicillata)", "Society finch (Lonchura striata domestica)", "Somali ostrich (Struthio molybdophanes)", "South African giraffe (Giraffa giraffa giraffa)", "South African ostrich (Struthio came														lus australis)", "South American (Nasua nasua) and White-nosed coatis (N. narica)", "Southern (Bucorvus leadbeateri) and Abyssinian ground hornbills (B. abyssinicus)", "Southern (Chauna torquata) and Northern screamers (C. chavaria)", "Southern (Tamandua tetradactyla) and Northern tamanduas (T. mexicana)", "Southern bluefin (Thunnus maccoyii)", "Pacific bluefin", "(T. orientalis)", "Bigeye (Thunnus obesus)", "Atlantic bluefin (T. thynnus)", "and Yellowfin tunas (T. albacares)", "Southern flounder (Paralichthys lethostigma) and Olive flounder (P. olivaceus)", "Southern white rhinoceros (Ceratotherium simum simum)", "Spanner crab (Ranina ranina)", "Spotted hyena (Crocuta crocuta)", "Springbok (Antidorcas marsupialis)", "Star finch (Neochmia ruficauda)", "Steppe lemming (Lagurus lagurus)", "Stingless bees (Melipona beecheii)", "(M. scutellaris)", "(M. bicolor)", "(M. quadrifasciata) and (M. subnitida)", "Stoat (Mustela erminea)", "Striped hyena (Hyaena hyaena)", "Sugar glider (Petaurus breviceps)", 														"Sugarbag bee (Tetragonula carbonaria)", "Sulphur-crested cockatoo (Cacatua galerita)", "Syrian wild ass (Equus hemionus hemippus)†", "Tarantula (Brachypelma spp. et al.)", "Tawny owl (Strix aluco)", "Thorold's deer (Cervus albirostris)", "Tiger quoll (Dasyurus maculatus)", "Eastern quoll (D. viverrinus)", "Western quoll (D. geoffroii) and Northern quoll (D. hallucatus)", "Turkish hamster (Mesocricetus brandti)", "Turquoise parrot (Neophema pulchella)", "Vicuña (Vicugna vicugna)", "Vulturine guineafowl (Acryllium vulturinum)", "Water buffalo (Bubalus bubalis)", "Water deer (Hydropotes inermis)", "Water flea (Daphnia magna)", "Wattle-necked softshell turtle (Palea steindachneri)", "Waxworm (Achroia grisella and Galleria mellonella)", "Webfoot octopus (Amphioctopus fangsiao)", "Wels catfish (Silurus glanis)", "Western honey bee (Apis mellifera)", "including subspecies Italian bee (A. mellifera ligustica)", "European dark bee (A. mellifera mellifera)", "Carniolan honey bee (A. mellifera carnica) and 													Caucasian honey bee (A. mellifera caucasia)", "Greek bee (A. mellifera cecropia)", "Western spotted skunk (Spilogale gracilis)", "White (Acipenser transmontanus)", "Shortnose (A. brevirostrum)", "Persian (A. persicus)", "Siberian (A. baerii)", "Adriatic (A. naccarii) and Starry sturgeon (A. stellatus)", "White cockatoo (Cacatua alba)", "White-necked (Corvus. albicollis)", "Common (C. corax)", "and Australian ravens (C. coronoides); Pied (C. albus)", "Carrion (C. corone)", "American (Corvus brachyrhynchos)", "and Hooded crows (C. cornix); Rook (C. frugilegus)", "White-tailed (Odocoileus virginianus) and Mule deer (O. hemionus)", "Whooper swan (Cygnus cygnus)", "Wild boar (Sus scrofa)", "Wood bison (Bison bison athabascae)", "Wood duck (Aix sponsa)", "Yak (Bos grunniens)", "Yellow-bellied glider (Petaurus australis)", "Yellow-crested and Citron-crested cockatoos (Cacatua sulphurea and C. S. citrinocristata)", "Yellowtail snapper (Ocyurus chrysurus)", "Zebra finch (Taeniopygia guttata)", "Zebu (Bos 														taurus indicus)" ];

											$.each(other, function (index, value) {

												$('#BreedOthers').append($('<option/>', {

													value: value,
													text : value 

												}));
											});

											</script>

								</datalist>

						</div>

						<div style = "float: left;">

							<button type = "button" class = "ButtonPet"> <img src = "../Resources/images/585e4adacb11b227491c3392.png" style = "width: 40%;"> </button>

						</div>

					</div>

				</div>

				<div class = "ContainerAnimals">

					<div style = "text-align: left; margin-left: 50px; margin-top: 40px;">

						<label class = "AnimalsLabel"> Dogs </label>

					</div>

					<div style = "margin-top: 50px;">

						<hr style = "border: 1.5px solid #AEB7BC; opacity: 1; width : 95%; margin: 0em auto;">

					</div>

					<div class = "scroll" style = "width: 100%; height: 500px; overflow: auto; padding-bottom: 50px;">

					<?php

						$querydogs = mysqli_query($conn, "SELECT * FROM pet_lost_dog") or die(mysqli_error($conn));

 						while ($row = mysqli_fetch_assoc($querydogs)) {

					?>

					<div>

						<div style = "float: left; margin-top: 50px; margin-left: 70px; width: 300px; height: 200px;box-shadow: 10px 10px 6px #000000CC; border-radius: 50px; background-image: url('../Server Uploads/Lost Dog/<?= $row['image']; ?>'); background-repeat: no-repeat; background-size: 100% 100%;">

							<div style = "width: 100%;">

								<button type = "button" style = "border: none; background: none; cursor: pointer; width: 100%; height: 100%; border-radius: 50px; opacity: 0;" class = "ButtonEdit" name = "ButtonEdit" data-toggle="modal" data-target="#ModalEditDog-<?= $row['id'] ?>"> Ini Button Ya </button>

								<form action = "UpdatePetDogs.php" method = "POST" enctype = "multipart/form-data">

									<div class="modal fade" id="ModalEditDog-<?= $row['id'] ?>" role="dialog">

    									<div class="modal-dialog">
    
      										<div class="modal-content">

        										<div class="modal-header">

          											<button type="button" class="close" data-dismiss="modal">&times;</button>
          											<h4 class="modal-title"> Update Animal </h4>

        										</div>

        										<div class="modal-body">

        											<div>

														<div style = "margin-left: 35px;" class="imageupload">

														<center>

															<label>

																<img id = "previewimages" style = "width: 400px;" src = "../Server Uploads/Lost Dog/<?= $row['image']; ?>">

															</label>

														</center>

														</div>


        											</div>

        											<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Date Arrived </label> </div>

            											<div style = "float: left; margin-left: 43px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "date" name = "DateArrivedUpdate" value = "<?= $row['datearrived'] ?>" required> </div>

           											</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Name </label> </div>

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "text" name = "AnimalNameUpdate" value = "<?= $row['name'] ?>" required> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Breed </label> </div>

            											<div style = "float: left; margin-left: 105px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "text" name = "BreedUpdate" value = "<?= $row['breed'] ?>" required> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Sex </label> </div>

            											<div style = "float: left; margin-left: 125px;"> <label class = "TulisanForm"> : </label> </div>

														<div style = "float: left; margin-left: 40px;"> <input type = "radio" name = "AnimalSexUpdate" <?=$row['sex']=="Male" ? "checked" : ""?> value = "Male"> <label class = "TulisanForm">  Male </label> </div>

            											<div style = "float: left; margin-left: 40px;"> <input type = "radio" name = "AnimalSexUpdate" <?=$row['sex']=="Female" ? "checked" : ""?> value = "Female"> <label class = "TulisanForm"> Female </label> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Age </label> </div>

            											<div style = "float: left; margin-left: 120px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "number" name = "AnimalAgeUpdate" value = "<?= $row['age'] ?>" required> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Description </label> </div>

            											<div style = "float: left; margin-left: 55px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <textarea name = "DescriptionUpdate" required> <?= $row['description'] ?> </textarea> </div>

       												</div>

												</div>

												<div class="modal-footer">

												</div>

      										</div>
      
    									</div>

									</div>

								</form>

							</div>

						</div>

					</div>

					<?php

						}

					?>

					</div>

					<div>

						<hr style = "border: 1.5px solid #AEB7BC; opacity: 1; width : 95%; margin: 0em auto;">

					</div>

					<div style = "text-align: left; margin-left: 50px; margin-top: 40px;">

						<label class = "AnimalsLabel"> Cats </label>

					</div>

					<div style = "margin-top: 50px;">

						<hr style = "border: 1.5px solid #AEB7BC; opacity: 1; width : 95%; margin: 0em auto;">

					</div>

					<div class = "scroll" style = "width: 100%; height: 500px; overflow: auto; padding-bottom: 50px;">

					<?php

						$querycats = mysqli_query($conn, "SELECT * FROM pet_lost_cat") or die(mysqli_error($conn));

 						while ($row = mysqli_fetch_assoc($querycats)) {

					?>

					<div>

						<div style = "float: left; margin-top: 50px; margin-left: 70px; width: 300px; height: 200px;box-shadow: 10px 10px 6px #000000CC; border-radius: 50px; background-image: url('../Server Uploads/Lost Cat/<?= $row['image']; ?>'); background-repeat: no-repeat; background-size: 100% 100%;">

							<div style = "width: 100%;">

								<button type = "button" style = "border: none; background: none; cursor: pointer; width: 100%; height: 100%; border-radius: 50px; opacity: 0;" class = "ButtonEdit" name = "ButtonEdit" data-toggle="modal" data-target="#ModalEditCat-<?= $row['id'] ?>">Ini Button Ya </button>

								<form action = "UpdatePetDogs.php" method = "POST" enctype = "multipart/form-data">

									<div class="modal fade" id="ModalEditCat-<?= $row['id'] ?>" role="dialog">

    									<div class="modal-dialog">
    
      										<div class="modal-content">

        										<div class="modal-header">

          											<button type="button" class="close" data-dismiss="modal">&times;</button>
          											<h4 class="modal-title"> Update Animal </h4>

        										</div>

        										<div class="modal-body">

        											<div>

														<div style = "margin-left: 35px;" class="imageupload">

														<center>

															<label>

																<img id = "previewimages" style = "width: 400px;" src = "../Server Uploads/Lost Cat/<?= $row['image']; ?>">

															</label>

														</center>

														</div>


        											</div>

        											<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Date Arrived </label> </div>

            											<div style = "float: left; margin-left: 43px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "date" name = "DateArrivedUpdate" value = "<?= $row['datearrived'] ?>" required> </div>

           											</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Name </label> </div>

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "text" name = "AnimalNameUpdate" value = "<?= $row['name'] ?>" required> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Breed </label> </div>

            											<div style = "float: left; margin-left: 105px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "text" name = "BreedUpdate" value = "<?= $row['breed'] ?>" required> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Sex </label> </div>

            											<div style = "float: left; margin-left: 125px;"> <label class = "TulisanForm"> : </label> </div>

														<div style = "float: left; margin-left: 40px;"> <input type = "radio" name = "AnimalSexUpdate" <?=$row['sex']=="Male" ? "checked" : ""?> value = "Male"> <label class = "TulisanForm">  Male </label> </div>

            											<div style = "float: left; margin-left: 40px;"> <input type = "radio" name = "AnimalSexUpdate" <?=$row['sex']=="Female" ? "checked" : ""?> value = "Female"> <label class = "TulisanForm"> Female </label> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Age </label> </div>

            											<div style = "float: left; margin-left: 120px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "number" name = "AnimalAgeUpdate" value = "<?= $row['age'] ?>" required> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Description </label> </div>

            											<div style = "float: left; margin-left: 55px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <textarea name = "DescriptionUpdate" required> <?= $row['description'] ?> </textarea> </div>

       												</div>

												</div>

												<div class="modal-footer">

												</div>

      										</div>
      
    									</div>

									</div>

								</form>

							</div>

						</div>

					</div>

					<?php

						}

					?>

					</div>

					<div>

						<hr style = "border: 1.5px solid #AEB7BC; opacity: 1; width : 95%; margin: 0em auto;">

					</div>

					<div style = "text-align: left; margin-left: 50px; margin-top: 40px;">

						<label class = "AnimalsLabel"> Other Animals </label>

					</div>

					<div style = "margin-top: 50px;">

						<hr style = "border: 1.5px solid #AEB7BC; opacity: 1; width : 95%; margin: 0em auto;">

					</div>

					<div class = "scroll" style = "width: 100%; height: 500px; overflow: auto; padding-bottom: 50px;">

					<?php

						$queryother = mysqli_query($conn, "SELECT * FROM pet_lost_other") or die(mysqli_error($conn));

 						while ($row = mysqli_fetch_assoc($queryother)) {

					?>

					<div>

						<div style = "float: left; margin-top: 50px; margin-left: 70px; width: 300px; height: 200px;box-shadow: 10px 10px 6px #000000CC; border-radius: 50px; background-image: url('../Server Uploads/Lost Other/<?= $row['image']; ?>'); background-repeat: no-repeat; background-size: 100% 100%;">

							<div style = "width: 100%;">

								<button type = "button" style = "border: none; background: none; cursor: pointer; width: 100%; height: 100%; border-radius: 50px; opacity: 0;" class = "ButtonEdit" name = "ButtonEdit" data-toggle="modal" data-target="#ModalEditOther-<?= $row['id'] ?>">Ini Button Ya </button>

								<form action = "UpdatePetDogs.php" method = "POST" enctype = "multipart/form-data">

									<div class="modal fade" id="ModalEditOther-<?= $row['id'] ?>" role="dialog">

    									<div class="modal-dialog">
    
      										<div class="modal-content">

        										<div class="modal-header">

          											<button type="button" class="close" data-dismiss="modal">&times;</button>
          											<h4 class="modal-title"> Update Animal </h4>

        										</div>

        										<div class="modal-body">

        											<div>

														<div style = "margin-left: 35px;" class="imageupload">

														<center>

															<label>

																<img id = "previewimages" style = "width: 400px;" src = "../Server Uploads/Lost Other/<?= $row['image']; ?>">

															</label>

														</center>

														</div>


        											</div>

        											<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Date Arrived </label> </div>

            											<div style = "float: left; margin-left: 43px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "date" name = "DateArrivedUpdate" value = "<?= $row['datearrived'] ?>" required> </div>

           											</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Name </label> </div>

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "text" name = "AnimalNameUpdate" value = "<?= $row['name'] ?>" required> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Breed </label> </div>

            											<div style = "float: left; margin-left: 105px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "text" name = "BreedUpdate" value = "<?= $row['breed'] ?>" required> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Sex </label> </div>

            											<div style = "float: left; margin-left: 125px;"> <label class = "TulisanForm"> : </label> </div>

														<div style = "float: left; margin-left: 40px;"> <input type = "radio" name = "AnimalSexUpdate" <?=$row['sex']=="Male" ? "checked" : ""?> value = "Male"> <label class = "TulisanForm">  Male </label> </div>

            											<div style = "float: left; margin-left: 40px;"> <input type = "radio" name = "AnimalSexUpdate" <?=$row['sex']=="Female" ? "checked" : ""?> value = "Female"> <label class = "TulisanForm"> Female </label> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Age </label> </div>

            											<div style = "float: left; margin-left: 120px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <input type = "number" name = "AnimalAgeUpdate" value = "<?= $row['age'] ?>" required> </div>

       												</div>

													<div style = "margin-top: 20px; height: 35px;">

            											<div style = "float: left; margin-left: 100px;"> <label class = "TulisanForm"> Description </label> </div>

            											<div style = "float: left; margin-left: 55px;"> <label class = "TulisanForm"> : </label> </div>

           												<div style = "float: left; margin-left: 40px;"> <textarea name = "DescriptionUpdate" required> <?= $row['description'] ?> </textarea> </div>

       												</div>

												</div>

												<div class="modal-footer">

												</div>

      										</div>
      
    									</div>

									</div>

								</form>

							</div>

						</div>

					</div>

					<?php

						}

					?>

					</div>

					<div>

						<hr style = "border: 1.5px solid #AEB7BC; opacity: 1; width : 95%; margin: 0em auto;">

					</div>

					<br>
					<br>
					<br>

				</div>

				</fieldset>

		<div>

			<div class = "bgFooter">

				<div style = "float: right;">

					<div style = "margin-right: 70px; margin-top: 50px;"> 

						<label class = "LabelLearn"> © Copyright 2019, powered by puppet </label> 

					</div>

				</div>

				<div style = "display: inline-block; margin-top: 380px; ">

					
					<div style = "margin-top: 20px; float: right; margin-left: 140px;"> 

						<a href = "#" class = "LabelMore"> <b> Learn More </b> </a> 

					</div>

					<div style = "margin-top: 20px; float: right; margin-left: 110px;"> 

						<a href = "#" class = "LabelMore"> <b> Our Shelter </b> </a> 

					</div>

					<div style = "margin-top: 20px; float: right; margin-left: 130px;"> 

						<a href = "#" class = "LabelMore"> <b> Contact Us </b> </a> 

					</div>

					<div style = "margin-top: 20px; float: right; margin-left: 250px;"> 

						<a href = "#" class = "LabelMore"> <b> About Us </b> </a> 

					</div>

				</div>

			</div>

		</div>

	</body>

</html>