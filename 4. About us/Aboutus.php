<html>

	<head>
	
		<title> About Us </title>

		<style>

			* {

				margin : 0;
				padding : 0;
			}

			@font-face {

				font-family: "acremedium";

				src: url('../Resources/fonts/jonathan_ball_-_acre-medium-webfont.woff2') format('woff2'),

				url('../Resources/fonts/jonathan_ball_-_acre-medium-webfont.woff') format('woff');

				font-weight: normal;

				font-style: normal;

			}

			@font-face {

				font-family : "holtwood_one_scregular";

				src : url("../Resources/fonts/holtwoodonesc-webfont.woff2") format("woff2"),

				url("../Resources/fonts/holtwoodonesc-webfont.woff") format("woff");
				font-weight : normal;

				font-style : normal;

			}

			@font-face {

				font-family: 'autour_oneregular';
				src: url('../Resources/fonts/autourone-regular-webfont.woff2') format('woff2'),
				url('../Resources/fonts/autourone-regular-webfont.woff') format('woff');
				font-weight: normal;
				font-style: normal;

			}

			@font-face {

				font-family: 'primerprint';
				src: url('../Resources/fonts/primer_print-webfont.woff2') format('woff2'),
				url('../Resources/fonts/primer_print-webfont.woff') format('woff');
				font-weight: normal;
				font-style: normal;

			}

			.BackgroundAtas {

				width : 100%;
				height: 300%;
				background-image: url("../Resources/images/Violet+Simple+Class+Schedule_upscaled_illustration_x4.png");
				background-position: center;
				background-repeat: no-repeat;
				background-size: cover;

			}

			.ContainerNavbar {

				height: 10px;

			}

			.NavbarKiri {

				float: left;

			}

			.NavbarKanan {

				margin-top: 40px;
				width: 800px;
				float: right;

			}

			.TulisanNavbar {

				font-family: acremedium;
				text-decoration: none;
				font-size: 33px;
				letter-spacing: 0;
				color: #E54A4A;
				opacity: 1;

			}

			.TulisanNavbar:hover {

				font-family: acremedium;
				text-decoration: none;
				font-size: 33px;
				letter-spacing: 0;
				color: #F34D14;
				opacity: 1;

			}

			.TulisanNavbarTerpilih {

				font-family: acremedium;
				text-decoration: none;
				font-size: 33px;
				letter-spacing: 0;
				color: #4AA7E5;
				opacity: 1;

			}

			.LogoUtama {

				margin-top : 40px;
				margin-left : 50px;
				width : 150px;
				-webkit-filter : drop-shadow(0px 3px 3px #000000);
				filter : drop-shadow(0px 3px 3px #000000); 

			}

			fieldset {

				width : 1200px;
				height: 1800px;
				margin-top: 440px;
				background : #FFFFFF 0% 0% no-repeat padding-box;
				box-shadow : 10px 10px 6px #0000004F;
				border : 2px solid #FFA200;
				border-radius : 25px;
				opacity : 1;

			}

			.ContainerLogo {

				margin-top : 40px;
				margin-left : 200px;
				position : relative;
				width : 200px;
				height : 230px;
				position: absolute;
				top: 210px;
				right: 100px;

			}

			.ContainerLogoDepan {

				position: absolute;
				z-index: 2;
			}

			.ContainerLogoBelakang {

				position: absolute;
				z-index: 1;
				left: -500px;
				width: 500px;
			}

			.LogoKucing {

				width : 110px;
				-webkit-filter : drop-shadow(0px 3px 3px #000000);
				filter : drop-shadow(0px 3px 3px #000000);

			}

			.IconRegister {

				font-family : holtwood_one_scregular;
				letter-spacing : -3px;
				color : #4C9AE9;
				text-shadow : 0px 8px 4px #00000070;
				font-size : 60px;
				position : absolute;
				margin-top : 75px;
				right : -104px;

			}

			::-webkit-input-placeholder {

				color: #00000029;
				font-family : acremedium;
				font-size : 15px;

			}

			.bgFooter {

 				background-image: url("../Resources/images/footer.jpg");
				background-repeat: no-repeat;
				background-size: 100% 100%;
				width : 100%;
				height: 550px;
			}
	
			.LabelLearn {

				color: #FEBF56;
				font-family: primerprint;
				font-size: 15px;

			}

			.LabelMore {

				color: black;
				font-family: primerprint;
				font-size: 42px;
				text-decoration: none;

			}

			.LabelMore:hover {

				color: #FFAA00;
				font-family: primerprint;
				font-size: 42px;
				text-decoration: none;

			}

			.LabelLove {

				color: black;
				font-family: autour_oneregular;
				font-size: 44px;

			}

			.LabelAboutUs {

				color: #4AA7E5;
				font-family : acremedium;
				font-size : 60px;

			}

			.ImageAbout {

				box-shadow: 10px 10px 6px #000000B3;
				margin-top: 30px;
				width: 80%;

			}

			.HeaderAbout {

				color: #E54A4A;
				font-family : acremedium;
				font-size : 26px;

			}

			.LabelAbout {

				color: #66747E;
				font-family : acremedium;
				font-size : 24px;


			}

			.AAbout {

				color: #1C99F2;
				font-family : acremedium;
				font-size : 24px;
				text-decoration: none;

			}

		</style>

	</head>

	<body>

		<div class = "BackgroundAtas">

			<div class = "ContainerNavbar">

				<div class = "NavbarKiri">

					<a href = "..\1. Home\Home.php"> <img src = "../Resources/images/LOGOANML.png" class = "LogoUtama"> </a>

				</div>

				<div class = "NavbarKanan">

					<div style = "float: left;"> 

						<center>

							<a href = "..\2. Adopt A Pet\Adoptapet.php" class = "TulisanNavbar"> Adopt <br> a pet </a> 

						</center>

					</div>

					<div style = "float: left; margin-left: 85px;">

						<center>

							<a href = "..\3. Lost and Found\Lostandfound.php" class = "TulisanNavbar"> Lost <br> and found </a>

						</center>

					</div>

					<div style = "float: left; margin-left: 85px; margin-top: 20px;">

						<center>

							<a href = "Aboutus.php" class = "TulisanNavbarTerpilih"> About Us </a>

						</center>

					</div>

					<div style = "float: left; margin-left: 85px; margin-top: 20px;">

						<center>

							<a href = "..\5. Login\Login.php" class = "TulisanNavbar"> Login </a>

						</center>

					</div>

				</div>

			</div>

			<center>

				<div class = "ContainerLogo">

					<div class = "ContainerLogoDepan"> <img src = "../Resources/images/58a050065583a1291368eeb4.png" class = "LogoKucing"> </div>

					<div class = "ContainerLogoBelakang"> <label class = "IconRegister"> ABOUT US </label> </div>


				</div>

				<fieldset>

					<div style = "margin-top: 50px;">

						<label class = "LabelAboutUs"> About Us </label>

					</div>

					<div style = "margin-top: 20px;">

						<img src = "../Resources/images/Little-Girl-holding-puppy.jpg" class = "ImageAbout">

					</div>

					<div style = "text-align: left; width: 850px; margin-right: 105px;">

						<div style = "margin-top: 40px;">

							<label class = "HeaderAbout"> Compassion. Care. Community. </label>

						</div>

						<div style = "margin-top: 40px;">

							<label class = "LabelAbout"> We want to place adoptable shelter pets in loving, forever homes. With your help we can. </label>

						</div>

						<div style = "margin-top: 40px;">

							<label class = "LabelAbout"> At Love & Care Animals Shelter, we care about people and pets. In fact, we assist more thank 7.000 animals annually. As a function of the Pasadena Public Health Department, preventing the spread of disease is our number one mission. But we strive to achieve so much more. </label>

						</div>

						<div style = "margin-top: 40px;">

							<label class = "HeaderAbout"> Our Goals </label>

						</div>

						<div style = "margin-top: 40px;">

							<label class = "LabelAbout"> Our goal is to ensure that every pet we encounter stays in--or finds-- a loving home. Educating pet owners and potential pet owners ensures that loving home lasts for the lifetime of the cat or dog. </label>

						</div>

						<div style = "margin-top: 40px;">

							<label class = "LabelAbout"> Our mission is to provide responsive, efficient, and high-quality animal care and control services, while we promote, preserve, and protect public and animal safety through sheltering, pet placement programs, education, and humans law enforcement. </label>

						</div>

						<div style = "margin-top: 40px;">

							<label class = "HeaderAbout"> Getting Help For Animals In Need </label>

						</div>

						<div style = "margin-top: 40px;">

							<label class = "LabelAbout"> We care about people and pets. If you need us or if you know of an animal who needs us, help is just a phone call away at 0821-1073-0031. You can also fill out our non-emergency form to report an animal care issue to <a href = "#" class = "AAbout"> okisabeni@gmail.com </a> </label>

						</div>

					</div>

				</fieldset>

			</center>

		</div>

		<div class>

			<div class = "bgFooter">

				<div style = "float: right;">

					<div style = "margin-right: 70px; margin-top: 50px;"> 

						<label class = "LabelLearn"> © Copyright 2019, powered by puppet </label> 

					</div>

				</div>

				<div style = "display: inline-block; margin-top: 380px; ">

					
					<div style = "margin-top: 20px; float: right; margin-left: 140px;"> 

						<a href = "#" class = "LabelMore"> <b> Learn More </b> </a> 

					</div>

					<div style = "margin-top: 20px; float: right; margin-left: 110px;"> 

						<a href = "#" class = "LabelMore"> <b> Our Shelter </b> </a> 

					</div>

					<div style = "margin-top: 20px; float: right; margin-left: 130px;"> 

						<a href = "#" class = "LabelMore"> <b> Contact Us </b> </a> 

					</div>

					<div style = "margin-top: 20px; float: right; margin-left: 250px;"> 

						<a href = "#" class = "LabelMore"> <b> About Us </b> </a> 

					</div>

				</div>

			</div>

		</div>

	</body>

</html>